Aqui está um código complexo em Python que implementa um algoritmo de busca em largura em um grafo:

```python
from collections import deque

class Grafo:
    def __init__(self):
        self.vertices = {}

    def adicionar_vertice(self, vertice):
        if vertice not in self.vertices:
            self.vertices[vertice] = []

    def adicionar_aresta(self, u, v):
        if u in self.vertices and v in self.vertices:
            self.vertices[u].append(v)

    def buscar_em_largura(self, vertice_inicial):
        visitados = set()
        fila = deque([vertice_inicial])
        visitados.add(vertice_inicial)

        while fila:
            vertice_atual = fila.popleft()
            print(vertice_atual)

            for vizinho in self.vertices[vertice_atual]:
                if vizinho not in visitados:
                    visitados.add(vizinho)
                    fila.append(vizinho)

# Exemplo de uso
grafo = Grafo()
grafo.adicionar_vertice('A')
grafo.adicionar_vertice('B')
grafo.adicionar_vertice('C')
grafo.adicionar_vertice('D')
grafo.adicionar_vertice('E')

grafo.adicionar_aresta('A', 'B')
grafo.adicionar_aresta('B', 'C')
grafo.adicionar_aresta('C', 'D')
grafo.adicionar_aresta('D', 'E')
grafo.adicionar_aresta('E', 'A')

grafo.buscar_em_largura('A')
```

Este código cria uma classe `Grafo` que representa um grafo direcionado. Os vértices são armazenados em um dicionário, onde as chaves são os próprios vértices e os valores são listas de vértices adjacentes. O método `adicionar_vertice` é utilizado para adicionar um vértice ao grafo, e o método `adicionar_aresta` é utilizado para adicionar uma aresta entre dois vértices.

O método `buscar_em_largura` implementa o algoritmo de busca em largura no grafo. Ele começa visitando o vértice inicial, adiciona-o à fila de vértices a serem visitados e marca-o como visitado. Em um loop, o algoritmo retira o próximo vértice da fila, imprime-o e adiciona todos os seus vizinhos não visitados à fila. O processo continua até que a fila esteja vazia.

No exemplo de uso fornecido, um grafo com 5 vértices é criado e algumas arestas são adicionadas. Em seguida, o algoritmo de busca em largura é aplicado a partir do vértice 'A', imprimindo a ordem de visita dos vértices: 'A', 'B', 'C', 'D', 'E'.