Claro! Aqui está um exemplo de código complexo em Python que envolve manipulação de dados, estruturas de controle e funções:

```python
# Função para calcular o fatorial de um número
def fatorial(n):
    if n == 0:
        return 1
    else:
        return n * fatorial(n-1)

# Lista de números
numeros = [1, 2, 3, 4, 5]

# Dicionário para armazenar os fatoriais
fatoriais = {}

# Loop para calcular o fatorial de cada número e armazenar no dicionário
for num in numeros:
    fatoriais[num] = fatorial(num)

# Imprimir os fatoriais calculados
for num, fatorial_num in fatoriais.items():
    print(f'O fatorial de {num} é {fatorial_num}')
```

Nesse exemplo, temos uma função `fatorial` que calcula o fatorial de um número utilizando recursão. Em seguida, temos uma lista de números e um dicionário para armazenar os fatoriais calculados. O código utiliza um loop para calcular o fatorial de cada número da lista e armazenar no dicionário. Por fim, o código imprime os fatoriais calculados.