Segue um exemplo de código complexo em Scheme:

```scheme
;; Definição de uma função recursiva para calcular o fatorial de um número
(define (fatorial n)
  (if (= n 0)
      1
      (* n (fatorial (- n 1)))))

;; Definição de uma função para calcular o n-ésimo número da sequência de Fibonacci
(define (fibonacci n)
  (if (or (= n 0) (= n 1))
      n
      (+ (fibonacci (- n 1)) (fibonacci (- n 2)))))

;; Definição de uma função para inverter uma lista
(define (inverter-lista lst)
  (if (null? lst)
      '()
      (append (inverter-lista (cdr lst)) (list (car lst)))))

;; Definição de uma função de ordenação para uma lista
(define (ordenar-lista lst)
  (if (null? lst)
      '()
      (let ((menores (filter (lambda (x) (< x (car lst))) (cdr lst)))
            (maiores (filter (lambda (x) (>= x (car lst))) (cdr lst))))
        (append (ordenar-lista menores) (list (car lst)) (ordenar-lista maiores)))))

;; Exemplo de uso das funções
(displayln (fatorial 5)) ; Imprime o fatorial de 5 (120)
(displayln (fibonacci 7)) ; Imprime o 7º número da sequência de Fibonacci (13)
(displayln (inverter-lista '(1 2 3 4 5))) ; Imprime a lista invertida (5 4 3 2 1)
(displayln (ordenar-lista '(4 2 1 5 3))) ; Imprime a lista ordenada (1 2 3 4 5)
```

Nesse código, são definidas quatro funções: `fatorial`, `fibonacci`, `inverter-lista` e `ordenar-lista`. A função `fatorial` calcula o fatorial de um número usando recursão. A função `fibonacci` calcula o n-ésimo número da sequência de Fibonacci também usando recursão. A função `inverter-lista` inverte uma lista recursivamente. A função `ordenar-lista` ordena uma lista recursivamente usando o algoritmo de ordenação quicksort.

No exemplo de uso das funções, são feitas chamadas para calcular o fatorial de 5, o 7º número da sequência de Fibonacci, inverter uma lista e ordenar uma lista. Os resultados são então impressos no console.