Claro! Aqui está um exemplo de código de conexão MySQL em Python:

```python
import mysql.connector

# Crie uma conexão com o banco de dados
conexao = mysql.connector.connect(
    host="localhost",
    user="usuario",
    password="senha",
    database="nome_do_banco_de_dados"
)

# Verifique se a conexão foi estabelecida
if conexao.is_connected():
    print("Conexão estabelecida com sucesso!")

# Execute uma consulta no banco de dados
cursor = conexao.cursor()
cursor.execute("SELECT * FROM tabela")

# Recupere os resultados da consulta
resultados = cursor.fetchall()
for linha in resultados:
    print(linha)

# Encerre a conexão com o banco de dados
cursor.close()
conexao.close()
```

Lembre-se de substituir "localhost", "usuario", "senha", "nome_do_banco_de_dados" e "tabela" pelos valores correspondentes ao seu caso. Certifique-se de ter o pacote `mysql-connector-python` instalado para poder usar o módulo `mysql.connector`.