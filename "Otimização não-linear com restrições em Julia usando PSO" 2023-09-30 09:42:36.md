Segue um exemplo de código em Julia que realiza uma otimização não-linear com restrições usando o algoritmo de otimização por enxame de partículas (Particle Swarm Optimization):

```julia
using Random
using Optim

function rosenbrock(x)
    return (1 - x[1])^2 + 100 * (x[2] - x[1]^2)^2
end

function pso(obj, lb, ub, swarm_size, max_iter)
    dim = length(lb)
    global_best = Inf
    global_best_pos = zeros(dim)
    particle_best_pos = zeros((dim, swarm_size))
    particle_best_value = zeros(swarm_size)

    particles_pos = rand(dim, swarm_size) .* (ub - lb) .+ lb
    particles_vel = zeros((dim, swarm_size))

    for iter = 1:max_iter
        for i = 1:swarm_size
            value = obj(particles_pos[:, i])
            if value < particle_best_value[i]
                particle_best_value[i] = value
                particle_best_pos[:, i] = particles_pos[:, i]
                if value < global_best
                    global_best = value
                    global_best_pos = particles_pos[:, i]
                end
            end
        end

        for i = 1:swarm_size
            particles_vel[:, i] += 2 * rand() * (particle_best_pos[:, i] - particles_pos[:, i]) +
                2 * rand() * (global_best_pos - particles_pos[:, i])
            particles_pos[:, i] += particles_vel[:, i]
            particles_pos[:, i] = max.(min.(particles_pos[:, i], ub), lb)
        end
    end

    return global_best_pos, global_best
end

Random.seed!(123)
lb = [-5.0, -5.0]
ub = [5.0, 5.0]
swarm_size = 100
max_iter = 100

best_pos, best_value = pso(rosenbrock, lb, ub, swarm_size, max_iter)
println("Best position: ", best_pos)
println("Best value: ", best_value)
```

Neste exemplo, estamos otimizando a função de Rosenbrock em duas variáveis utilizando o algoritmo de otimização por enxame de partículas. A função `rosenbrock` calcula o valor da função de Rosenbrock em um determinado ponto. A função `pso` implementa o algoritmo de otimização por enxame de partículas, que atualiza a posição e a velocidade de cada partícula e encontra a melhor solução global. No final, o melhor resultado é exibido no terminal.