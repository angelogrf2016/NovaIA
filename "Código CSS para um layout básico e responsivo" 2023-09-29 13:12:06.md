Um código complexo em CSS seria o seguinte:

```css
body {
  background-color: #f5f5f5;
  font-family: Arial, sans-serif;
}

.container {
  width: 800px;
  margin: 0 auto;
  padding: 20px;
  background-color: white;
  border: 1px solid #ccc;
  box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
}

h1 {
  font-size: 24px;
  color: #333;
  margin-bottom: 20px;
}

p {
  font-size: 16px;
  color: #666;
  line-height: 1.5;
}

.button {
  display: inline-block;
  padding: 10px 20px;
  background-color: #3498db;
  color: white;
  text-decoration: none;
  border-radius: 5px;
  transition: background-color 0.3s ease;
}

.button:hover {
  background-color: #2980b9;
}

@media (max-width: 600px) {
  .container {
    width: 100%;
    padding: 10px;
  }

  h1 {
    font-size: 20px;
    margin-bottom: 10px;
  }

  p {
    font-size: 14px;
  }
}
```

Nesse código, temos um estilo básico para uma página HTML, com um contêiner centralizado na tela, um cabeçalho (`h1`), um parágrafo (`p`) e um botão estilizado (`button`). Além disso, também incluímos uma regra de media query para tornar o layout responsivo em telas menores.