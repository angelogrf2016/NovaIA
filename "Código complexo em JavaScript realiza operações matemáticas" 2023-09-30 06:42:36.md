Aqui está um exemplo de código complexo em JavaScript que realiza uma série de operações matemáticas:

```javascript
// Função para calcular o fatorial de um número
function calcularFatorial(num) {
  if (num === 0 || num === 1) {
    return 1;
  }
  return num * calcularFatorial(num - 1);
}

// Função para verificar se um número é primo
function verificarNumeroPrimo(num) {
  if (num <= 1) {
    return false;
  }
  for (let i = 2; i <= Math.sqrt(num); i++) {
    if (num % i === 0) {
      return false;
    }
  }
  return true;
}

// Função para calcular o quadrado de um número
function calcularQuadrado(num) {
  return num * num;
}

// Função para calcular a média de uma lista de números
function calcularMedia(listaNumeros) {
  const total = listaNumeros.reduce((acc, curr) => acc + curr, 0);
  return total / listaNumeros.length;
}

// Exemplo de utilização das funções
const numero = 5;
console.log(`O fatorial de ${numero} é: ${calcularFatorial(numero)}`);

const numeroPrimo = 7;
console.log(`O número ${numeroPrimo} é primo? ${verificarNumeroPrimo(numeroPrimo)}`);

const numeroQuadrado = 3;
console.log(`O quadrado de ${numeroQuadrado} é: ${calcularQuadrado(numeroQuadrado)}`);

const lista = [1, 2, 3, 4, 5];
console.log(`A média da lista ${lista} é: ${calcularMedia(lista)}`);
```

Este código contém quatro funções diferentes. A primeira função `calcularFatorial` calcula o fatorial de um número dado. A segunda função `verificarNumeroPrimo` verifica se um número é primo. A terceira função `calcularQuadrado` calcula o quadrado de um número. E a quarta função `calcularMedia` calcula a média de uma lista de números.

Em seguida, o código faz uso das funções para realizar cálculos específicos. Ele calcula o fatorial de um número, verifica se um número é primo, calcula o quadrado de um número e calcula a média de uma lista de números. Os resultados são impressos no console.