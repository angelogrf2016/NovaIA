Claro, aqui está um exemplo de código complexo em Lua:

```lua
-- Função recursiva para calcular o fatorial
function fatorial(n)
    if n == 0 then
        return 1
    else
        return n * fatorial(n - 1)
    end
end

-- Função para calcular o número de Fibonacci
function fibonacci(n)
    if n == 0 then
        return 0
    elseif n == 1 then
        return 1
    else
        return fibonacci(n - 1) + fibonacci(n - 2)
    end
end

-- Função para calcular o número de combinações
function combinacoes(n, k)
    return fatorial(n) / (fatorial(k) * fatorial(n - k))
end

-- Função para calcular o número de permutações
function permutacoes(n, k)
    return fatorial(n) / fatorial(n - k)
end

-- Função para verificar se um número é primo
function ehPrimo(n)
    if n <= 1 then
        return false
    end
    for i = 2, math.sqrt(n) do
        if n % i == 0 then
            return false
        end
    end
    return true
end

-- Exemplo de uso das funções
print("Fatorial de 5:", fatorial(5))
print("Fibonacci de 7:", fibonacci(7))
print("Combinações de 5 elementos tomados 3 a 3:", combinacoes(5, 3))
print("Permutações de 5 elementos tomados 3 a 3:", permutacoes(5, 3))
print("O número 17 é primo?", ehPrimo(17))
```

Neste exemplo, temos algumas funções matemáticas complexas implementadas em Lua. A função `fatorial` calcula o fatorial de um número recursivamente, a função `fibonacci` retorna o número de Fibonacci correspondente a um determinado índice, as funções `combinacoes` e `permutacoes` calculam o número de combinações e permutações de elementos, respectivamente, e a função `ehPrimo` verifica se um número é primo. No final, são mostrados exemplos de uso das funções.