Claro! Aqui está um código em Smalltalk que permite calcular o fatorial de um número fornecido pelo usuário:

```smalltalk
| numero fatorial |

Transcript cr; show: 'Digite um número: '.
numero := Sensor read.

fatorial := 1.
1 to: numero do: [:i |
    fatorial := fatorial * i.
].

Transcript cr; show: 'O fatorial de ', numero asString, ' é ', fatorial asString.
```

Neste código, o usuário é solicitado a digitar um número. Em seguida, o código utiliza um loop para calcular o fatorial desse número, armazenando o resultado na variável `fatorial`. Por fim, o resultado é exibido no console.