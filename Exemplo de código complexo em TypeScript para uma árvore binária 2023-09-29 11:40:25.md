Aqui está um exemplo de um código complexo em TypeScript que implementa uma estrutura de dados de uma árvore binária:

```typescript
class TreeNode<T> {
    value: T;
    left: TreeNode<T> | null;
    right: TreeNode<T> | null;

    constructor(value: T) {
        this.value = value;
        this.left = null;
        this.right = null;
    }
}

class BinaryTree<T> {
    root: TreeNode<T> | null;

    constructor() {
        this.root = null;
    }

    insert(value: T) {
        const newNode = new TreeNode<T>(value);

        if (this.root === null) {
            this.root = newNode;
        } else {
            this.insertNode(this.root, newNode);
        }
    }

    private insertNode(node: TreeNode<T>, newNode: TreeNode<T>) {
        if (newNode.value < node.value) {
            if (node.left === null) {
                node.left = newNode;
            } else {
                this.insertNode(node.left, newNode);
            }
        } else {
            if (node.right === null) {
                node.right = newNode;
            } else {
                this.insertNode(node.right, newNode);
            }
        }
    }

    find(value: T): boolean {
        return this.findNode(this.root, value);
    }

    private findNode(node: TreeNode<T> | null, value: T): boolean {
        if (node === null) {
            return false;
        }

        if (value === node.value) {
            return true;
        }

        if (value < node.value) {
            return this.findNode(node.left, value);
        } else {
            return this.findNode(node.right, value);
        }
    }

    remove(value: T) {
        this.root = this.removeNode(this.root, value);
    }

    private removeNode(node: TreeNode<T> | null, value: T): TreeNode<T> | null {
        if (node === null) {
            return null;
        }

        if (value < node.value) {
            node.left = this.removeNode(node.left, value);
            return node;
        } else if (value > node.value) {
            node.right = this.removeNode(node.right, value);
            return node;
        } else {
            if (node.left === null && node.right === null) {
                node = null;
                return node;
            }

            if (node.left === null) {
                node = node.right;
                return node;
            } else if (node.right === null) {
                node = node.left;
                return node;
            }

            const minValue = this.findMinValue(node.right);
            node.value = minValue;
            node.right = this.removeNode(node.right, minValue);
            return node;
        }
    }

    private findMinValue(node: TreeNode<T>): T {
        if (node.left === null) {
            return node.value;
        } else {
            return this.findMinValue(node.left);
        }
    }
}

// Exemplo de uso da árvore binária
const tree = new BinaryTree<number>();
tree.insert(8);
tree.insert(3);
tree.insert(10);
tree.insert(1);
tree.insert(6);
tree.insert(14);
tree.insert(4);
tree.insert(7);
tree.insert(13);

console.log(tree.find(6)); // Output: true

tree.remove(6);
console.log(tree.find(6)); // Output: false
```

Neste exemplo, criamos uma classe `TreeNode` que representa um nó da árvore binária e uma classe `BinaryTree` que é a própria árvore binária. A árvore binária é implementada com métodos para inserção, pesquisa e remoção de nós. No exemplo de uso, criamos uma árvore binária de números inteiros, inserimos alguns valores, pesquisamos um valor e removemos um valor.