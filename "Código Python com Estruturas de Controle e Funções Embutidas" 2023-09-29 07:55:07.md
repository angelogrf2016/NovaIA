Claro! Aqui está um exemplo de código complexo em Python que utiliza várias estruturas de controle e funções incorporadas:

```python
import random

def gerar_numeros_aleatorios(n):
    numeros = []
    for _ in range(n):
        numero = random.randint(1, 100)
        numeros.append(numero)
    return numeros

def calcular_media(numeros):
    soma = sum(numeros)
    media = soma / len(numeros)
    return media

def verificar_numeros_pares(numeros):
    pares = []
    for numero in numeros:
        if numero % 2 == 0:
            pares.append(numero)
    return pares

def contar_ocorrencias(numeros):
    ocorrencias = {}
    for numero in numeros:
        if numero in ocorrencias:
            ocorrencias[numero] += 1
        else:
            ocorrencias[numero] = 1
    return ocorrencias

def main():
    quantidade_numeros = int(input("Digite a quantidade de números aleatórios a gerar: "))
    numeros_aleatorios = gerar_numeros_aleatorios(quantidade_numeros)
    
    print("Números gerados:", numeros_aleatorios)
    
    media = calcular_media(numeros_aleatorios)
    print("Média dos números gerados:", media)
    
    pares = verificar_numeros_pares(numeros_aleatorios)
    print("Números pares gerados:", pares)
    
    ocorrencias = contar_ocorrencias(numeros_aleatorios)
    print("Ocorrências dos números gerados:", ocorrencias)

if __name__ == "__main__":
    main()
```

Este código gera uma lista de números aleatórios, calcula a média desses números, verifica quais são os números pares, e conta quantas vezes cada número ocorre na lista. No final, os resultados são impressos na saída padrão.