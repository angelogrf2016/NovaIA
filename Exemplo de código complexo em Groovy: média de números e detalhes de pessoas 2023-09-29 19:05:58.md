Aqui está um exemplo de código complexo em Groovy:

```groovy
// Função para calcular a média de um array de números
def calcularMedia(array) {
  def soma = 0
  array.each { numero ->
    soma += numero
  }
  return soma / array.size()
}

// Classe para representar uma pessoa
class Pessoa {
  String nome
  int idade
  
  // Construtor
  Pessoa(String nome, int idade) {
    this.nome = nome
    this.idade = idade
  }
  
  // Método para imprimir os detalhes da pessoa
  void imprimirDetalhes() {
    println("Nome: $nome")
    println("Idade: $idade")
  }
}

// Criar um array de números
def numeros = [1, 2, 3, 4, 5]

// Calcular a média dos números
def media = calcularMedia(numeros)
println("A média dos números é: $media")

// Criar uma lista de pessoas
def pessoas = [
  new Pessoa("João", 20),
  new Pessoa("Maria", 30),
  new Pessoa("Pedro", 25)
]

// Imprimir os detalhes de cada pessoa
pessoas.each { pessoa ->
  pessoa.imprimirDetalhes()
}
```

Neste exemplo, temos uma função `calcularMedia` que recebe um array de números como parâmetro e retorna a média desses números. Em seguida, temos uma classe `Pessoa` com um construtor e um método `imprimirDetalhes` para imprimir o nome e a idade de uma pessoa. O código cria um array de números, calcula a média e imprime o resultado. Também cria uma lista de pessoas, imprime os detalhes de cada pessoa e utiliza a função `calcularMedia` para calcular a média das idades das pessoas.