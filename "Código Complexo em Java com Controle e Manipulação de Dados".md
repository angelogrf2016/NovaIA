Claro! Aqui está um exemplo de código complexo em Java que utiliza várias estruturas de controle e manipulação de dados:

```java
import java.util.Scanner;

public class ComplexCode {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        // Declaração de variáveis
        int numero;
        int soma = 0;
        int maior = Integer.MIN_VALUE;
        int menor = Integer.MAX_VALUE;

        // Entrada de dados
        System.out.println("Digite 5 números inteiros:");

        for (int i = 0; i < 5; i++) {
            System.out.print("Número " + (i+1) + ": ");
            numero = scanner.nextInt();

            // Verifica se é o maior número
            if (numero > maior) {
                maior = numero;
            }

            // Verifica se é o menor número
            if (numero < menor) {
                menor = numero;
            }

            // Realiza a soma dos números
            soma += numero;
        }

        // Saída de dados
        System.out.println("A soma dos números é: " + soma);
        System.out.println("O maior número é: " + maior);
        System.out.println("O menor número é: " + menor);

        scanner.close();
    }
}
```

Neste exemplo, o código solicita ao usuário que digite 5 números inteiros e realiza algumas operações com esses números. Ele calcula a soma de todos os números, encontra o maior e o menor número digitado. No final, exibe esses resultados na tela.