Aqui está um exemplo de um código complexo em Kotlin que implementa uma estrutura de dados de árvore binária de busca:

```kotlin
class TreeNode(var value: Int) {
    var left: TreeNode? = null
    var right: TreeNode? = null
}

class BinarySearchTree {
    private var root: TreeNode? = null
    
    fun insert(value: Int) {
        root = insertRecursive(root, value)
    }
    
    private fun insertRecursive(current: TreeNode?, value: Int): TreeNode {
        if (current == null) {
            return TreeNode(value)
        }
        
        if (value < current.value) {
            current.left = insertRecursive(current.left, value)
        } else if (value > current.value) {
            current.right = insertRecursive(current.right, value)
        }

        return current
    }
    
    fun search(value: Int): Boolean {
        return searchRecursive(root, value)
    }
    
    private fun searchRecursive(current: TreeNode?, value: Int): Boolean {
        if (current == null) {
            return false
        }
        
        if (value == current.value) {
            return true
        }
        
        return if (value < current.value) {
            searchRecursive(current.left, value)
        } else {
            searchRecursive(current.right, value)
        }
    }
    
    fun delete(value: Int) {
        root = deleteRecursive(root, value)
    }
    
    private fun deleteRecursive(current: TreeNode?, value: Int): TreeNode? {
        if (current == null) {
            return null
        }
        
        if (value == current.value) {
            if (current.left == null && current.right == null) {
                return null
            }
            
            if (current.left == null) {
                return current.right
            }
            
            if (current.right == null) {
                return current.left
            }
            
            val smallestValue = findSmallestValue(current.right!!)
            current.value = smallestValue
            current.right = deleteRecursive(current.right, smallestValue)
            
            return current
        }
        
        if (value < current.value) {
            current.left = deleteRecursive(current.left, value)
        } else {
            current.right = deleteRecursive(current.right, value)
        }
        
        return current
    }
    
    private fun findSmallestValue(current: TreeNode): Int {
        return if (current.left == null) {
            current.value
        } else {
            findSmallestValue(current.left!!)
        }
    }
}
```

Este código implementa uma árvore binária de busca que permite inserir elementos, buscar elementos e excluir elementos. A classe `TreeNode` representa um nó da árvore com um valor, um filho esquerdo e um filho direito. A classe `BinarySearchTree` implementa as operações de inserção, busca e exclusão na árvore.

Este é apenas um exemplo de um código complexo em Kotlin, existem muitas outras estruturas de dados e algoritmos que podem ser implementados em Kotlin.