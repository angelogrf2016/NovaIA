Aqui está um exemplo de um código em Ruby que envolve a criação de uma classe, herança, métodos e manipulação de objetos:

```ruby
class Animal
  attr_accessor :name, :age

  def initialize(name, age)
    @name = name
    @age = age
  end

  def speak
    puts "O animal está fazendo barulho!"
  end
end

class Dog < Animal
  def initialize(name, age, breed)
    super(name, age)
    @breed = breed
  end

  def speak
    puts "O cachorro #{@name} está latindo!"
  end

  def wag_tail
    puts "O cachorro #{@name} está abanando o rabo!"
  end
end

class Cat < Animal
  def initialize(name, age, color)
    super(name, age)
    @color = color
  end

  def speak
    puts "O gato #{@name} está miando!"
  end

  def purr
    puts "O gato #{@name} está ronronando!"
  end
end

# Criando objetos e chamando os métodos
dog = Dog.new("Rex", 5, "Labrador")
dog.speak
dog.wag_tail

cat = Cat.new("Whiskers", 3, "Cinza")
cat.speak
cat.purr
```

Neste exemplo, temos uma classe Animal que possui um nome e uma idade, e um método `speak` que imprime uma mensagem genérica. A classe Dog herda de Animal e adiciona uma raça específica de cachorro, além de sobrescrever o método `speak` para imprimir uma mensagem específica para cachorro. A classe Cat também herda de Animal, adicionando uma cor específica para gatos e sobrescrevendo o método `speak` para imprimir uma mensagem específica para gato.

Ao criar objetos das classes Dog e Cat, podemos chamar os métodos `speak`, `wag_tail` e `purr` para cada objeto, respectivamente.