Aqui está um exemplo de código SQL complexo que envolve várias tabelas e operações:

```
SELECT c.nome AS cliente, p.nome AS produto, SUM(ip.quantidade) AS total_vendido
FROM clientes c
INNER JOIN vendas v ON c.id = v.cliente_id
INNER JOIN itens_pedido ip ON v.id = ip.venda_id
INNER JOIN produtos p ON ip.produto_id = p.id
WHERE v.data >= '2021-01-01' AND v.data <= '2021-12-31'
GROUP BY c.nome, p.nome
HAVING SUM(ip.quantidade) > 100
ORDER BY total_vendido DESC;
```

Explicação do código:

1. O código começa selecionando as colunas `c.nome`, `p.nome` e a soma da coluna `ip.quantidade` (total vendido) e atribuindo aliases para cada uma delas.
2. Em seguida, são feitos vários joins para relacionar várias tabelas: `clientes`, `vendas`, `itens_pedido` e `produtos`.
3. O join `INNER JOIN` é usado para combinar registros onde as chaves correspondentes existem em ambas as tabelas.
4. As condições `ON` especificam como as tabelas estão relacionadas.
5. A cláusula `WHERE` é usada para filtrar os dados. Neste exemplo, apenas as vendas realizadas entre 1º de janeiro de 2021 e 31 de dezembro de 2021 são consideradas.
6. A cláusula `GROUP BY` é usada para agrupar os resultados pelo nome do cliente e do produto.
7. A cláusula `HAVING` é usada para filtrar os resultados agrupados. Neste exemplo, apenas os produtos com uma quantidade total vendida maior que 100 são retornados.
8. A cláusula `ORDER BY` é usada para classificar os resultados pelo total vendido em ordem decrescente.