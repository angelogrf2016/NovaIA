Claro! Aqui está um exemplo de código VHDL complexo que utiliza uma estrutura de dados personalizada para implementar um algoritmo de ordenação chamado Merge Sort:

```vhdl
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity Merge_Sort is
    generic (
        N : positive := 8
    );
    port (
        clk : in std_logic;
        reset : in std_logic;
        start : in std_logic;
        inputs : in std_logic_vector(N-1 downto 0);
        outputs : out std_logic_vector(N-1 downto 0);
        done : out std_logic
    );
end entity Merge_Sort;

architecture behavioral of Merge_Sort is
    type array_type is array (natural range <>) of std_logic_vector(N-1 downto 0);
    signal sorted_array : array_type(0 to N-1);
    
    procedure MergeSort(input_array : in array_type; output_array : out array_type) is
        variable mid : natural;
        variable left_array : array_type(0 to N-1);
        variable right_array : array_type(0 to N-1);
        variable i, j, k : natural;
    begin
        if N > 1 then
            mid := N/2;
            for i in 0 to mid-1 loop
                left_array(i) := input_array(i);
            end loop;
            for i in mid to N-1 loop
                right_array(i-mid) := input_array(i);
            end loop;
            MergeSort(left_array, left_array);
            MergeSort(right_array, right_array);
            
            i := 0;
            j := 0;
            k := 0;
            while i < mid and j < N-mid loop
                if left_array(i) <= right_array(j) then
                    output_array(k) := left_array(i);
                    i := i + 1;
                else
                    output_array(k) := right_array(j);
                    j := j + 1;
                end if;
                k := k + 1;
            end loop;
            
            while i < mid loop
                output_array(k) := left_array(i);
                i := i + 1;
                k := k + 1;
            end loop;
            
            while j < N-mid loop
                output_array(k) := right_array(j);
                j := j + 1;
                k := k + 1;
            end loop;
        else
            output_array(0) := input_array(0);
        end if;
    end procedure MergeSort;
    
    signal sorting : boolean := false;
    signal sorted : boolean := false;
begin
    process(clk, reset)
    begin
        if reset = '1' then
            sorting <= false;
            sorted <= false;
            done <= '0';
        elsif rising_edge(clk) then
            if start = '1' and not sorting then
                sorting <= true;
                sorted <= false;
                done <= '0';
                MergeSort(inputs, sorted_array);
                sorting <= false;
                sorted <= true;
                done <= '1';
            end if;
        end if;
    end process;
    
    process(sorted)
    begin
        if sorted then
            outputs <= sorted_array(0);
            for i in 1 to N-1 loop
                outputs <= outputs & sorted_array(i);
            end loop;
        else
            outputs <= (others => '0');
        end if;
    end process;
end architecture;
```

Este código VHDL implementa o algoritmo de ordenação Merge Sort. Ele define uma entidade chamada "Merge_Sort" com entrada para o clock (clk), sinal de reset (reset), sinal de início (start), vetor de entrada (inputs), vetor de saída (outputs) e sinal de conclusão (done). A entidade possui um parâmetro genérico N para definir o tamanho do vetor de entrada e saída.

A arquitetura "behavioral" implementa o algoritmo de ordenação Merge Sort usando uma estrutura de dados personalizada chamada "array_type", que é um array de vetores lógicos de tamanho N. O procedimento "MergeSort" é responsável por realizar a ordenação. Ele divide o vetor de entrada em duas metades, aplica recursivamente o algoritmo Merge Sort em cada metade e depois mescla as metades ordenadas.

A arquitetura também possui sinais internos para controlar o processo de ordenação e armazenar o vetor ordenado. O processo principal é sensível às bordas de subida do clock e ao sinal de reset. Quando o sinal de início é ativado e não há processo de ordenação em andamento, o processo inicia o algoritmo de ordenação Merge Sort, atualizando o sinal de ordenação, ordenando o vetor de entrada e atualizando os sinais de conclusão. O segundo processo é sensível ao sinal "sorted" e atualiza o vetor de saída com o vetor ordenado quando o processo de ordenação está completo.

Espero que este código atenda às suas expectativas e lhe forneça um exemplo interessante e desafiador em VHDL!