Claro! Aqui está um exemplo de código complexo em Go que implementa uma árvore de busca binária:

```go
package main

import (
	"fmt"
)

type Node struct {
	Value  int
	Left   *Node
	Right  *Node
}

type BinaryTree struct {
	Root *Node
}

func (bt *BinaryTree) Insert(value int) {
	if bt.Root == nil {
		bt.Root = &Node{Value: value}
	} else {
		bt.Root.insert(value)
	}
}

func (n *Node) insert(value int) {
	if value < n.Value {
		if n.Left == nil {
			n.Left = &Node{Value: value}
		} else {
			n.Left.insert(value)
		}
	} else {
		if n.Right == nil {
			n.Right = &Node{Value: value}
		} else {
			n.Right.insert(value)
		}
	}
}

func (bt *BinaryTree) Search(value int) bool {
	return bt.Root.search(value)
}

func (n *Node) search(value int) bool {
	if n == nil {
		return false
	}

	if value == n.Value {
		return true
	} else if value < n.Value {
		return n.Left.search(value)
	} else {
		return n.Right.search(value)
	}
}

func main() {
	tree := BinaryTree{}

	tree.Insert(5)
	tree.Insert(3)
	tree.Insert(7)
	tree.Insert(1)
	tree.Insert(4)
	tree.Insert(6)
	tree.Insert(8)

	fmt.Println(tree.Search(4)) // true
	fmt.Println(tree.Search(10)) // false
}
```

Neste código, temos a definição de uma estrutura `Node` que representa um nó da árvore e uma estrutura `BinaryTree` que define a árvore em si. O método `Insert` permite inserir um valor na árvore, enquanto o método `Search` permite pesquisar um valor na árvore.

A árvore é implementada como uma árvore de busca binária, onde os valores menores são inseridos à esquerda e os valores maiores são inseridos à direita. A pesquisa é realizada percorrendo a árvore de forma recursiva, comparando o valor desejado com o valor do nó atual.

No exemplo de código, criamos uma árvore binária, inserimos alguns valores e realizamos algumas pesquisas para verificar se os valores estão presentes na árvore. A saída do programa será `true` para a pesquisa do valor 4 (que está presente na árvore) e `false` para a pesquisa do valor 10 (que não está presente na árvore).