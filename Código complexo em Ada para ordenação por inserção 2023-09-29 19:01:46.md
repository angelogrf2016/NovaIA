Aqui está um exemplo de um código complexo em ADA que implementa um algoritmo de ordenação por inserção em uma matriz:

```ada
with Ada.Text_IO; use Ada.Text_IO;

procedure Insertion_Sort is
   type Array is array (Integer range <>) of Integer;

   procedure Sort (A : in out Array) is
   begin
      for I in 2 .. A'Last loop
         declare
            Temp : Integer := A (I);
            J    : Integer := I - 1;
         begin
            while J >= 1 and then A (J) > Temp loop
               A (J + 1) := A (J);
               J := J - 1;
            end loop;
            A (J + 1) := Temp;
         end;
      end loop;
   end Sort;

   A : Array (1 .. 10) := (9, 5, 2, 8, 1, 7, 3, 6, 10, 4);

begin
   Put ("Array before sorting: ");
   for I in A'Range loop
      Put (Integer'Image (A (I)) & " ");
   end loop;
   New_Line;

   Sort (A);

   Put ("Array after sorting: ");
   for I in A'Range loop
      Put (Integer'Image (A (I)) & " ");
   end loop;
   New_Line;
end Insertion_Sort;
```

Este código implementa o algoritmo de ordenação por inserção em uma matriz de inteiros. A função `Sort` recebe uma matriz como entrada e a ordena em ordem crescente usando o algoritmo de ordenação por inserção.

A matriz é inicializada com valores desordenados e, em seguida, é exibida antes e depois de ser ordenada usando o algoritmo de ordenação por inserção.

Espero que esse código atenda às suas expectativas!