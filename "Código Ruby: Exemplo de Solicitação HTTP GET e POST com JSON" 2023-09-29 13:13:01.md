Claro! Aqui está um exemplo de código complexo em Ruby:

```ruby
require 'net/http'
require 'json'

# Função para fazer uma solicitação GET HTTP
def get(uri)
  url = URI.parse(uri)
  http = Net::HTTP.new(url.host, url.port)
  request = Net::HTTP::Get.new(url.request_uri)
  response = http.request(request)
  JSON.parse(response.body)
end

# Função para fazer uma solicitação POST HTTP
def post(uri, data)
  url = URI.parse(uri)
  http = Net::HTTP.new(url.host, url.port)
  request = Net::HTTP::Post.new(url.request_uri)
  request.body = data.to_json
  response = http.request(request)
  JSON.parse(response.body)
end

# Exemplo de uso das funções

# Faz uma solicitação GET para uma API e imprime o resultado
response = get('https://jsonplaceholder.typicode.com/posts')
puts response

# Faz uma solicitação POST para a mesma API, enviando dados
data = { title: 'Teste', body: 'Isso é um teste', userId: 1 }
response = post('https://jsonplaceholder.typicode.com/posts', data)
puts response
```

Este código usa as bibliotecas `net/http` e `json` para fazer solicitações HTTP GET e POST e manipular dados JSON. Ele faz uma solicitação GET para uma API RESTful (https://jsonplaceholder.typicode.com/posts) e imprime o resultado. Em seguida, faz uma solicitação POST para a mesma API, enviando um objeto JSON com dados fictícios e imprime o resultado novamente.

Observe que este é apenas um exemplo simples de uso das bibliotecas `net/http` e `json` e que existem muitas outras funcionalidades disponíveis para manipulação de solicitações HTTP e manipulação de JSON em Ruby.