Aqui está um exemplo de código em C++ que implementa um algoritmo de ordenação chamado Merge Sort:

```cpp
#include <iostream>
using namespace std;

void merge(int arr[], int left[], int leftSize, int right[], int rightSize) {
    int i = 0, j = 0, k = 0;
    
    while (i < leftSize && j < rightSize) {
        if (left[i] <= right[j]) {
            arr[k] = left[i];
            i++;
        } else {
            arr[k] = right[j];
            j++;
        }
        k++;
    }
    
    while (i < leftSize) {
        arr[k] = left[i];
        i++;
        k++;
    }
    
    while (j < rightSize) {
        arr[k] = right[j];
        j++;
        k++;
    }
}

void mergeSort(int arr[], int size) {
    if (size < 2) {
        return;
    }
    
    int mid = size / 2;
    int left[mid];
    int right[size - mid];
    
    for (int i = 0; i < mid; i++) {
        left[i] = arr[i];
    }
    
    for (int i = mid; i < size; i++) {
        right[i - mid] = arr[i];
    }
    
    mergeSort(left, mid);
    mergeSort(right, size - mid);
    merge(arr, left, mid, right, size - mid);
}

int main() {
    int arr[] = {5, 2, 9, 1, 7, 6, 3};
    int size = sizeof(arr) / sizeof(arr[0]);
    
    cout << "Array antes da ordenação: ";
    for (int i = 0; i < size; i++) {
        cout << arr[i] << " ";
    }
    cout << endl;
    
    mergeSort(arr, size);
    
    cout << "Array depois da ordenação: ";
    for (int i = 0; i < size; i++) {
        cout << arr[i] << " ";
    }
    cout << endl;
    
    return 0;
}
```

Este código implementa o algoritmo de ordenação Merge Sort. Ele começa dividindo o array em duas partes iguais e, em seguida, divide cada parte novamente até que cada subarray tenha apenas um elemento. Em seguida, ele combina os subarrays em ordem crescente até que o array original esteja completamente ordenado.

O código começa definindo duas funções: `merge` e `mergeSort`. A função `merge` é responsável por combinar dois subarrays em ordem crescente, enquanto a função `mergeSort` é a função principal que divide e combina os subarrays até que o array inteiro esteja ordenado.

No `main`, um array de inteiros é definido e passado para a função `mergeSort`. Em seguida, o array é impresso antes e depois da ordenação para verificar o resultado.