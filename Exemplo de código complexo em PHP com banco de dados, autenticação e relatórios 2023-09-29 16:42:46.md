Claro! Aqui está um exemplo de código complexo em PHP que envolve o uso de bancos de dados, autenticação de usuários e geração de relatórios:

```php
<?php

// Conectar ao banco de dados
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "exemplo";

$conn = new mysqli($servername, $username, $password, $dbname);

// Verificar conexão
if ($conn->connect_error) {
    die("Falha na conexão: " . $conn->connect_error);
}

// Autenticar usuário
if (isset($_POST['login']) && isset($_POST['senha'])) {
    $login = $_POST['login'];
    $senha = $_POST['senha'];

    $sql = "SELECT * FROM usuarios WHERE login = '$login' AND senha = '$senha'";
    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
        echo "Usuário autenticado com sucesso!";
    } else {
        echo "Usuário ou senha inválidos!";
    }
}

// Gerar relatório
$sql = "SELECT * FROM pedidos";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    echo "<table>";
    echo "<tr><th>ID</th><th>Cliente</th><th>Produto</th></tr>";

    while ($row = $result->fetch_assoc()) {
        echo "<tr><td>" . $row["id"] . "</td><td>" . $row["cliente"] . "</td><td>" . $row["produto"] . "</td></tr>";
    }

    echo "</table>";
} else {
    echo "Nenhum pedido encontrado.";
}

$conn->close();

?>
```

Neste exemplo, conectamos ao banco de dados MySQL, autenticamos o usuário com base em suas credenciais fornecidas por meio de um formulário HTML e geramos um relatório com os dados da tabela "pedidos". Certifique-se de substituir as informações de conexão ao banco de dados com as suas próprias.