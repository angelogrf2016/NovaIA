Claro! Aqui está um exemplo de código complexo em Ruby:
```ruby
require 'prime'

def factorial(n)
  Prime.each(n).reduce(:*)
end

def is_perfect_square?(n)
  Math.sqrt(n).to_i == Math.sqrt(n)
end

def fibonacci(n)
  return 0 if n == 0
  return 1 if n == 1

  fib = [0, 1]
  (2..n).each do |i|
    fib << fib[i - 1] + fib[i - 2]
  end

  fib.last
end

def sum_of_digits(n)
  n.to_s.chars.map(&:to_i).reduce(:+)
end

def palindrome?(str)
  str.downcase == str.downcase.reverse
end

def prime_factors(n)
  Prime.prime_division(n).flat_map { |factor, power| [factor] * power }
end

def find_duplicates(arr)
  arr.select { |element| arr.count(element) > 1 }.uniq
end

def calculate_average(arr)
  arr.reduce(:+) / arr.length.to_f
end

# Exemplo de uso
puts factorial(5) # Output: 120
puts is_perfect_square?(49) # Output: true
puts fibonacci(8) # Output: 21
puts sum_of_digits(12345) # Output: 15
puts palindrome?("racecar") # Output: true
puts prime_factors(36) # Output: [2, 2, 3, 3]
puts find_duplicates([1, 2, 3, 2, 4, 5, 4, 6]) # Output: [2, 4]
puts calculate_average([1, 2, 3, 4, 5]) # Output: 3.0
```
Este código contém várias funções que realizam diferentes operações matemáticas e em listas. Ele calcula o fatorial de um número, verifica se um número é um quadrado perfeito, calcula o n-ésimo número da sequência de Fibonacci, soma os dígitos de um número, verifica se uma string é um palíndromo, encontra os fatores primos de um número, encontra elementos duplicados em uma lista, e calcula a média de uma lista de números.