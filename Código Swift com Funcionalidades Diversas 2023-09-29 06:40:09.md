Aqui está um exemplo de código complexo em Swift que envolve várias funcionalidades:

```swift
import UIKit

class ComplexCode {
    func calculateFactorial(of number: Int) -> Int {
        guard number > 0 else {
            return 1
        }
        
        return number * calculateFactorial(of: number - 1)
    }
    
    func isPalindrome(_ string: String) -> Bool {
        let reversedString = String(string.reversed())
        return string.lowercased() == reversedString.lowercased()
    }
    
    func countOccurrences(of character: Character, in string: String) -> Int {
        var count = 0
        
        for char in string {
            if char == character {
                count += 1
            }
        }
        
        return count
    }
}

let code = ComplexCode()

// Calcular o fatorial de um número
let factorial = code.calculateFactorial(of: 5)
print("O fatorial de 5 é \(factorial)")

// Verificar se uma string é um palíndromo
let isPalindrome = code.isPalindrome("Ana")
print("A string 'Ana' é um palíndromo? \(isPalindrome)")

// Contar a ocorrência de um caractere em uma string
let count = code.countOccurrences(of: "a", in: "banana")
print("O caractere 'a' ocorre \(count) vezes na string 'banana'")
```

Neste exemplo, a classe `ComplexCode` contém três métodos: `calculateFactorial(of:)` para calcular o fatorial de um número, `isPalindrome(_:)` para verificar se uma string é um palíndromo e `countOccurrences(of:in:)` para contar a ocorrência de um caractere em uma string. 

Após a definição da classe, instanciamos um objeto `code` e utilizamos os métodos para realizar diferentes operações. No exemplo acima, calculamos o fatorial de 5, verificamos se a string "Ana" é um palíndromo e contamos a ocorrência do caractere "a" na string "banana". Os resultados são exibidos no console.