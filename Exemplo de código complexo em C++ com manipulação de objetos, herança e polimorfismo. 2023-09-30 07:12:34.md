Claro! Aqui está um exemplo de código complexo em C++ que envolve manipulação de objetos, herança e polimorfismo:

```cpp
#include <iostream>
#include <vector>

// Classe base
class Animal {
public:
    virtual void fazerSom() = 0; // Método virtual puro

    void dormir() {
        std::cout << "Zzz..." << std::endl;
    }
};

// Classe derivada
class Cachorro : public Animal {
public:
    void fazerSom() {
        std::cout << "Au au!" << std::endl;
    }
};

// Classe derivada
class Gato : public Animal {
public:
    void fazerSom() {
        std::cout << "Miau!" << std::endl;
    }
};

int main() {
    // Criação de objetos
    Cachorro cachorro;
    Gato gato;

    // Chamada dos métodos
    cachorro.fazerSom(); // Output: "Au au!"
    gato.fazerSom();     // Output: "Miau!"

    // Uso de polimorfismo
    std::vector<Animal*> animais;
    animais.push_back(&cachorro);
    animais.push_back(&gato);

    for (auto animal : animais) {
        animal->fazerSom();
    }

    return 0;
}
```

Neste exemplo, temos uma classe base `Animal` que possui um método virtual puro `fazerSom()` e um método `dormir()`. Em seguida, temos duas classes derivadas `Cachorro` e `Gato`, que implementam o método `fazerSom()` de forma diferente.

No `main()`, criamos objetos do tipo `Cachorro` e `Gato` e chamamos o método `fazerSom()` de cada um. Depois, utilizamos polimorfismo ao criar um vetor de ponteiros para `Animal` e adicionamos os objetos `Cachorro` e `Gato` a ele. Em seguida, percorremos o vetor e chamamos o método `fazerSom()` para cada animal, aproveitando a capacidade do polimorfismo em chamar a implementação correta de acordo com o tipo do objeto.

O resultado será a impressão dos sons "Au au!", "Miau!", "Au au!" e "Miau!" no console.