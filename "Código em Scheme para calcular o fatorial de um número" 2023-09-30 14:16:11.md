Claro! Aqui está um código em Scheme que implementa uma função recursiva para calcular o fatorial de um número:

```scheme
(define (fatorial n)
  (if (<= n 1)
      1
      (* n (fatorial (- n 1)))))

(display "Digite um número para calcular o fatorial: ")
(define num (read))
(display "O fatorial de ")
(display num)
(display " é ")
(display (fatorial num))
```

Neste código, definimos uma função chamada `fatorial` que recebe um número `n`. Se `n` for menor ou igual a 1, retornamos 1, caso contrário, multiplicamos `n` pelo fatorial de `n-1`. 

Em seguida, solicitamos ao usuário que digite um número para calcular o fatorial. O número digitado é armazenado na variável `num`. Por fim, exibimos o resultado do fatorial do número digitado.

Lembre-se de que Scheme é uma linguagem funcional e a recursão é uma técnica comumente usada para resolver problemas nessa linguagem.