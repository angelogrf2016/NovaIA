Claro! Aqui está um código em LUA que realiza a criptografia e descriptografia de uma mensagem utilizando o algoritmo de Cifra de César:

```lua
-- Função para criptografar uma mensagem usando a Cifra de César
function encrypt(message, shift)
  local encrypted = ""
  
  for i = 1, #message do
    local char = message:sub(i, i)
    
    if char >= 'a' and char <= 'z' then
      char = string.char((char:byte() - 97 + shift) % 26 + 97)
    elseif char >= 'A' and char <= 'Z' then
      char = string.char((char:byte() - 65 + shift) % 26 + 65)
    end
    
    encrypted = encrypted .. char
  end
  
  return encrypted
end

-- Função para descriptografar uma mensagem usando a Cifra de César
function decrypt(message, shift)
  return encrypt(message, -shift)
end

-- Mensagem a ser criptografada
local message = "Hello, World!"
local shift = 3

-- Criptografar a mensagem
local encryptedMessage = encrypt(message, shift)
print("Mensagem criptografada: " .. encryptedMessage)

-- Descriptografar a mensagem
local decryptedMessage = decrypt(encryptedMessage, shift)
print("Mensagem descriptografada: " .. decryptedMessage)
```

Neste código, a função `encrypt` recebe uma mensagem e um valor de deslocamento (shift) como parâmetros. Ela itera sobre cada caractere da mensagem e, se for uma letra minúscula ou maiúscula, realiza a cifra de César, deslocando o caractere no alfabeto de acordo com o valor do deslocamento. A função `decrypt` utiliza a função `encrypt` para descriptografar a mensagem, invertendo o valor do deslocamento. Por fim, o código criptografa a mensagem "Hello, World!" com um deslocamento de 3 e imprime a mensagem criptografada e descriptografada.