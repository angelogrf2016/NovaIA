import scala.util.Random

object CodigoDiferenciado {
  def main(args: Array[String]): Unit = {
    val numeros = List(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)
    val letras = List('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j')
    
    val resultado = gerarCombinacoes(numeros, letras)
    
    println(resultado)
  }
  
  def gerarCombinacoes(numeros: List[Int], letras: List[Char]): List[String] = {
    val combinacoes = for {
      n <- numeros
      l <- letras
      random = Random.nextInt(100)
      if random % 2 == 0
    } yield {
      s"$n$l$random"
    }
    
    combinacoes
  }
}

Neste código, temos uma função chamada `gerarCombinacoes` que recebe duas listas como parâmetros: `numeros` e `letras`. A função faz um loop pelos elementos das duas listas e, para cada combinação, gera um número aleatório. Se o número for par, adiciona a combinação de número, letra e número aleatório em uma nova lista de combinações.

A função `gerarCombinacoes` é chamada no método `main`, utilizando duas listas de exemplo: `numeros` contendo os números de 1 a 10 e `letras` contendo as letras de 'a' a 'j'. O resultado das combinações é então impresso no console.

O código utiliza a biblioteca `scala.util.Random` para gerar números aleatórios. O critério para adicionar uma combinação na lista final é que o número aleatório seja par. Portanto, cada vez que o código for executado, as combinações geradas podem variar, tornando-o diferenciado e difícil de se repetir.