Aqui está um exemplo de um código complexo em JavaScript que envolve a criação de uma classe, manipulação de arrays e funções assíncronas:

```javascript
// Criação da classe Pessoa
class Pessoa {
  constructor(nome, idade) {
    this.nome = nome;
    this.idade = idade;
  }

  async saudacao() {
    return new Promise(resolve => {
      setTimeout(() => {
        resolve(`Olá, meu nome é ${this.nome} e tenho ${this.idade} anos.`);
      }, 1000);
    });
  }
}

// Criação de um array de pessoas
const pessoas = [
  new Pessoa('João', 20),
  new Pessoa('Maria', 25),
  new Pessoa('Pedro', 30)
];

// Função assíncrona para imprimir as saudações de todas as pessoas
async function imprimirSaudacoes() {
  for (const pessoa of pessoas) {
    const saudacao = await pessoa.saudacao();
    console.log(saudacao);
  }
}

// Chamada da função assíncrona
imprimirSaudacoes();
```

Neste código, criamos uma classe chamada `Pessoa` que possui um construtor para definir o nome e idade da pessoa, e um método assíncrono `saudacao()` que retorna uma promessa que resolve com uma saudação contendo o nome e idade da pessoa.

Em seguida, criamos um array de objetos `pessoas` contendo instâncias da classe `Pessoa`.

Por fim, definimos uma função assíncrona `imprimirSaudacoes()` que utiliza um loop `for...of` para percorrer o array de pessoas e, para cada pessoa, aguarda a resolução da promessa retornada pelo método `saudacao()` e imprime a saudação no console.

A chamada da função `imprimirSaudacoes()` é feita no final do código para que as saudações sejam impressas no console.