Claro! Aqui está um exemplo de código complexo em Ruby que envolve a manipulação de arquivos, processamento de dados e uso de classes:

```ruby
class Pessoa
  attr_accessor :nome, :idade
  
  def initialize(nome, idade)
    @nome = nome
    @idade = idade
  end
  
  def imprimir_dados
    puts "Nome: #{@nome}"
    puts "Idade: #{@idade}"
  end
end

def ler_arquivo(nome_arquivo)
  pessoas = []
  
  if File.exist?(nome_arquivo)
    File.foreach(nome_arquivo) do |linha|
      dados = linha.chomp.split(",")
      pessoa = Pessoa.new(dados[0], dados[1].to_i)
      pessoas << pessoa
    end
  else
    puts "O arquivo não existe."
  end
  
  pessoas
end

def escrever_arquivo(nome_arquivo, pessoas)
  File.open(nome_arquivo, "w") do |arquivo|
    pessoas.each do |pessoa|
      arquivo.puts "#{pessoa.nome},#{pessoa.idade}"
    end
  end
  
  puts "Arquivo #{nome_arquivo} atualizado com sucesso."
end

def filtrar_pessoas(pessoas, idade_minima)
  pessoas_filtradas = pessoas.select { |pessoa| pessoa.idade >= idade_minima }
  pessoas_filtradas
end

# Cria um novo arquivo com algumas pessoas
escrever_arquivo("pessoas.txt", [Pessoa.new("João", 30), Pessoa.new("Maria", 25), Pessoa.new("Pedro", 40)])

# Lê o arquivo e imprime os dados das pessoas
pessoas = ler_arquivo("pessoas.txt")
pessoas.each { |pessoa| pessoa.imprimir_dados }

# Filtra as pessoas com idade mínima de 30 e imprime os dados novamente
pessoas_filtradas = filtrar_pessoas(pessoas, 30)
pessoas_filtradas.each { |pessoa| pessoa.imprimir_dados }
```

Este código cria uma classe `Pessoa` com atributos `nome` e `idade`, e um método `imprimir_dados` para exibir essas informações na saída padrão. Em seguida, há três métodos adicionais: `ler_arquivo`, `escrever_arquivo` e `filtrar_pessoas`.

O método `ler_arquivo` lê um arquivo no formato "nome,idade" e retorna uma lista de objetos `Pessoa` com base nos dados lidos. O método `escrever_arquivo` recebe uma lista de objetos `Pessoa` e escreve os dados no arquivo no mesmo formato.

Por fim, o método `filtrar_pessoas` recebe uma lista de objetos `Pessoa` e uma idade mínima, e retorna uma nova lista contendo apenas as pessoas cuja idade é maior ou igual à idade mínima.

O código então cria um arquivo chamado "pessoas.txt" com algumas pessoas, lê os dados desse arquivo, imprime as informações, filtra as pessoas com idade mínima de 30 e imprime novamente os dados das pessoas filtradas.