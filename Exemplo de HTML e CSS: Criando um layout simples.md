<!DOCTYPE html>
<html lang="pt-br">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Exemplo de HTML e CSS</title>
  <style>
    body {
      font-family: Arial, sans-serif;
      background-color: #f2f2f2;
      color: #333;
    }

    h1 {
      text-align: center;
      color: #ff6600;
      margin-top: 50px;
    }

    p {
      font-size: 18px;
      line-height: 1.5;
      margin-bottom: 20px;
    }

    .container {
      max-width: 800px;
      margin: 0 auto;
      padding: 20px;
      background-color: #fff;
      box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
    }

    .btn {
      display: inline-block;
      padding: 10px 20px;
      background-color: #ff6600;
      color: #fff;
      text-decoration: none;
      border-radius: 5px;
      transition: background-color 0.3s ease;
    }

    .btn:hover {
      background-color: #cc5500;
    }
  </style>
</head>
<body>
  <div class="container">
    <h1>Bem-vindo ao exemplo de HTML e CSS!</h1>
    <p>Este é um exemplo de código HTML e CSS em português. Aqui você pode ver como criar um layout simples utilizando as linguagens de marcação e estilização mais utilizadas na web.</p>
    <p>Utilizamos as tags HTML para estruturar o conteúdo da página, como os cabeçalhos (h1), parágrafos (p) e divs (container). Já as propriedades CSS são responsáveis por definir o estilo visual da página, como a cor de fundo, fonte e espaçamento.</p>
    <p>Além disso, também adicionamos um botão estilizado utilizando a classe .btn do CSS para dar destaque a uma chamada para ação.</p>
    <p>Aproveite este exemplo para aprender mais sobre HTML e CSS e criar seus próprios projetos web!</p>

    <a href="#" class="btn">Clique aqui</a>
  </div>
</body>
</html>