Claro! Aqui está um exemplo de código complexo em F# que envolve o uso de tipos avançados, expressões lambda e manipulação de coleções:

```fsharp
open System

// Definindo um tipo de dados complexo
type Pessoa = {
    Nome: string
    Idade: int
    Salario: decimal
}

// Função para calcular o aumento de salário de uma pessoa
let calcularAumento (pessoa: Pessoa) =
    if pessoa.Salario < 1000m then
        { pessoa with Salario = pessoa.Salario * 1.1m }
    else if pessoa.Salario < 2000m then
        { pessoa with Salario = pessoa.Salario * 1.05m }
    else
        { pessoa with Salario = pessoa.Salario * 1.02m }

// Lista de pessoas
let pessoas = [
    { Nome = "João"; Idade = 30; Salario = 1500m }
    { Nome = "Maria"; Idade = 25; Salario = 800m }
    { Nome = "Carlos"; Idade = 40; Salario = 2500m }
]

// Aplicando o aumento de salário em todas as pessoas da lista
let pessoasComAumento = List.map calcularAumento pessoas

// Imprimindo as informações das pessoas com o novo salário
pessoasComAumento
|> List.iter (fun pessoa ->
    printfn "Nome: %s | Idade: %d | Salário: %f" pessoa.Nome pessoa.Idade pessoa.Salario)
```

Neste código, definimos um tipo de dados chamado `Pessoa` que possui três propriedades: `Nome`, `Idade` e `Salario`. Em seguida, temos uma função chamada `calcularAumento` que recebe uma pessoa como argumento e calcula o aumento de salário com base em algumas condições.

Em seguida, criamos uma lista de pessoas e aplicamos a função `calcularAumento` em cada uma delas usando a função `List.map`. O resultado é armazenado na variável `pessoasComAumento`.

Por fim, imprimimos as informações das pessoas com o novo salário usando a função `List.iter` e uma expressão lambda para formatar a saída.

Espero que este exemplo atenda às suas expectativas de um código complexo em F#! Se você tiver alguma dúvida, é só perguntar.