Aqui está um exemplo de código complexo em Swift que cria um aplicativo de lista de tarefas com recursos de adicionar, remover e marcar tarefas como concluídas:

```swift
import UIKit

struct Task {
    var title: String
    var completed: Bool
}

class TaskListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var tasks: [Task] = []
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var taskTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    @IBAction func addTaskButtonPressed(_ sender: UIButton) {
        if let taskTitle = taskTextField.text, !taskTitle.isEmpty {
            let newTask = Task(title: taskTitle, completed: false)
            tasks.append(newTask)
            tableView.reloadData()
            taskTextField.text = ""
        }
    }
    
    // MARK: - UITableViewDataSource Methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tasks.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TaskCell", for: indexPath)
        let task = tasks[indexPath.row]
        cell.textLabel?.text = task.title
        cell.accessoryType = task.completed ? .checkmark : .none
        return cell
    }
    
    // MARK: - UITableViewDelegate Methods
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        tasks[indexPath.row].completed.toggle()
        tableView.reloadRows(at: [indexPath], with: .automatic)
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            tasks.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
    }
}
```

Este código cria uma classe `Task` que representa uma tarefa com um título e um estado de conclusão. Em seguida, a classe `TaskListViewController` é criada, que é uma subclasse de `UIViewController` e implementa os métodos necessários para a exibição e manipulação de uma lista de tarefas.

O código cria uma tabela (`UITableView`) para exibir as tarefas e um campo de texto (`UITextField`) para inserir novas tarefas. Quando o botão "Adicionar" é pressionado, uma nova tarefa é criada com base no texto inserido e adicionada à lista de tarefas. A tabela é então recarregada para exibir a nova tarefa e o campo de texto é limpo.

Além disso, o código implementa os métodos `tableView(_:numberOfRowsInSection:)` e `tableView(_:cellForRowAt:)` para exibir a lista de tarefas na tabela. As tarefas concluídas são exibidas com uma marca de seleção e as não concluídas sem a marca de seleção. Ao selecionar uma tarefa, ela é marcada como concluída ou não concluída, e a tabela é recarregada para refletir essa alteração. Ao deslizar para excluir uma tarefa, ela é removida da lista de tarefas e a tabela é atualizada.