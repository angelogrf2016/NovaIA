Claro! Aqui está um exemplo de código de conexão com um banco de dados MySQL em JavaScript usando o pacote mysql2:

```javascript
const mysql = require('mysql2');

// Configurações da conexão com o banco de dados
const connection = mysql.createConnection({
  host: 'localhost', // Endereço do banco de dados
  user: 'root', // Usuário do banco de dados
  password: 'senha', // Senha do banco de dados
  database: 'mydatabase' // Nome do banco de dados
});

// Conecta ao banco de dados
connection.connect((err) => {
  if (err) {
    console.error('Erro ao conectar ao banco de dados: ', err);
    return;
  }
  console.log('Conexão bem-sucedida ao banco de dados!');
  
  // Aqui você pode executar consultas, inserções, atualizações, exclusões, etc.
  
  // Encerra a conexão com o banco de dados
  connection.end();
});
```

Tenha certeza de substituir `'localhost'`, `'root'`, `'senha'` e `'mydatabase'` pelas suas próprias configurações.