Aqui está um exemplo de código para conecta-se a um banco de dados MySQL usando HTML e JavaScript:

```html
<!DOCTYPE html>
<html>
<head>
    <title>Conexão MySQL</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
</head>
<body>
    <h1>Conexão MySQL</h1>

    <script>
        // Configuração da conexão com o MySQL
        var config = {
            host: 'localhost',
            user: 'seu_usuario',
            password: 'sua_senha',
            database: 'nome_do_banco_de_dados'
        };

        // Função para realizar a conexão
        function connectToMySQL() {
            $.ajax({
                url: 'connect.php', // Arquivo PHP para realizar a conexão
                type: 'POST',
                data: config,
                success: function(response) {
                    console.log('Conexão realizada com sucesso!');
                    // Aqui você pode fazer qualquer operação com o banco de dados
                },
                error: function(error) {
                    console.log('Erro ao conectar ao MySQL: ' + error);
                }
            });
        }

        // Chamando a função de conexão
        connectToMySQL();
    </script>
</body>
</html>
```

Para utilizar este código, você precisará criar um arquivo `connect.php` com o seguinte conteúdo:

```php
<?php
// Recuperando os dados da requisição POST
$host = $_POST['host'];
$user = $_POST['user'];
$password = $_POST['password'];
$database = $_POST['database'];

// Estabelecendo a conexão com o MySQL
$mysqli = new mysqli($host, $user, $password, $database);

// Verificando se ocorreu algum erro na conexão
if ($mysqli->connect_error) {
    die('Erro ao conectar ao MySQL: ' . $mysqli->connect_error);
}

// Retornando uma resposta de sucesso
echo 'Conexão realizada com sucesso!';
?>
```

Certifique-se de substituir `'seu_usuario'`, `'sua_senha'` e `'nome_do_banco_de_dados'` pelos dados corretos para a sua configuração do MySQL.