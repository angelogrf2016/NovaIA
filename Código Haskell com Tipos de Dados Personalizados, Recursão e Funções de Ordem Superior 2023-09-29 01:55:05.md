Claro! Aqui está um exemplo de código Haskell que envolve o uso de tipos de dados personalizados, recursão e funções de ordem superior:

```haskell
-- Definição de uma árvore binária
data ArvoreBinaria a = No a (ArvoreBinaria a) (ArvoreBinaria a) | Folha deriving Show

-- Função que encontra a soma de todos os elementos de uma árvore binária
somaArvore :: Num a => ArvoreBinaria a -> a
somaArvore Folha = 0
somaArvore (No x esq dir) = x + somaArvore esq + somaArvore dir

-- Função de ordem superior que aplica uma função a todos os elementos de uma árvore binária
mapArvore :: (a -> b) -> ArvoreBinaria a -> ArvoreBinaria b
mapArvore _ Folha = Folha
mapArvore f (No x esq dir) = No (f x) (mapArvore f esq) (mapArvore f dir)

-- Exemplo de utilização
arvoreExemplo :: ArvoreBinaria Int
arvoreExemplo = No 1 (No 2 (No 3 Folha Folha) (No 4 Folha Folha)) (No 5 Folha Folha)

main :: IO ()
main = do
  putStrLn $ "Árvore: " ++ show arvoreExemplo
  putStrLn $ "Soma dos elementos: " ++ show (somaArvore arvoreExemplo)
  let arvoreDobrada = mapArvore (* 2) arvoreExemplo
  putStrLn $ "Árvore dobrada: " ++ show arvoreDobrada
```

Neste exemplo, definimos um tipo de dados `ArvoreBinaria` que representa uma árvore binária. Essa árvore pode ser construída como um nó contendo um valor e duas subárvores (esquerda e direita), ou como uma folha. Em seguida, definimos a função `somaArvore` que calcula a soma de todos os elementos da árvore, utilizando recursão para percorrer a estrutura. Também definimos a função `mapArvore`, que aplica uma função a todos os elementos da árvore, retornando uma nova árvore com os resultados.

No exemplo principal, criamos uma árvore de exemplo e a exibimos na tela. Em seguida, calculamos a soma de seus elementos com a função `somaArvore` e exibimos o resultado. Por fim, utilizamos a função `mapArvore` para dobrar todos os elementos da árvore e exibimos o resultado.