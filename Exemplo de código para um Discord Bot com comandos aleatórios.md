Claro! Aqui está um exemplo de código para um Discord Bot aleatório com alguns elementos e comandos aleatórios:

```python
import discord
from discord.ext import commands
import random

# Definir o token do bot
TOKEN = 'SEU_TOKEN'

# Definir o prefixo dos comandos
bot = commands.Bot(command_prefix='!')

# Evento quando o bot estiver pronto
@bot.event
async def on_ready():
    print(f'Bot conectado como {bot.user.name}')

# Comando para rolar um dado
@bot.command()
async def roll(ctx):
    dice = random.randint(1, 6)
    await ctx.send(f'O dado caiu em: {dice}')

# Comando para enviar uma imagem aleatória de gato
@bot.command()
async def cat(ctx):
    cat_images = ['https://example.com/cat1.jpg', 'https://example.com/cat2.jpg', 'https://example.com/cat3.jpg']
    random_cat = random.choice(cat_images)
    await ctx.send(random_cat)

# Comando para enviar um meme aleatório
@bot.command()
async def meme(ctx):
    meme_images = ['https://example.com/meme1.jpg', 'https://example.com/meme2.jpg', 'https://example.com/meme3.jpg']
    random_meme = random.choice(meme_images)
    await ctx.send(random_meme)

# Comando para dar um "oi" aleatório
@bot.command()
async def oi(ctx):
    greetings = ['Oi!', 'Olá!', 'E aí!', 'Oi, tudo bem?']
    random_greeting = random.choice(greetings)
    await ctx.send(random_greeting)

# Comando para contar até um número aleatório
@bot.command()
async def count(ctx):
    count_number = random.randint(1, 10)
    for i in range(1, count_number + 1):
        await ctx.send(i)

# Executar o bot
bot.run(TOKEN)
```

Lembre-se de substituir 'SEU_TOKEN' pelo token do seu bot. Este código cria um bot que pode rolar um dado, enviar uma imagem aleatória de gato, enviar um meme aleatório, dar um "oi" aleatório e contar até um número aleatório. Você pode adicionar mais comandos e elementos aleatórios conforme desejar.