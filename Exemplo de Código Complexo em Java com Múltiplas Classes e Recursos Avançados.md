Aqui está um exemplo de código complexo em Java que envolve múltiplas classes e recursos avançados:

```java
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        // Criação de objetos
        Animal animal = new Animal("Leão", 10);
        Pessoa pessoa = new Pessoa("João", 25);
        List<Animal> animais = new ArrayList<>();
        animais.add(animal);

        // Utilização de métodos
        pessoa.adotarAnimais(animais);
        pessoa.exibirDados();
        pessoa.exibirAnimaisAdotados();

        animal.fazerBarulho();
    }
}

class Animal {
    private String nome;
    private int idade;

    public Animal(String nome, int idade) {
        this.nome = nome;
        this.idade = idade;
    }

    public void fazerBarulho() {
        System.out.println("O animal " + nome + " está fazendo barulho!");
    }
}

class Pessoa {
    private String nome;
    private int idade;
    private List<Animal> animaisAdotados;

    public Pessoa(String nome, int idade) {
        this.nome = nome;
        this.idade = idade;
        this.animaisAdotados = new ArrayList<>();
    }

    public void adotarAnimais(List<Animal> animais) {
        animaisAdotados.addAll(animais);
    }

    public void exibirDados() {
        System.out.println("Nome: " + nome);
        System.out.println("Idade: " + idade);
    }

    public void exibirAnimaisAdotados() {
        System.out.println("Animais adotados:");
        for (Animal animal : animaisAdotados) {
            System.out.println(animal);
        }
    }
}
```

Neste exemplo, temos uma classe `Animal` que representa um animal com nome e idade, e uma classe `Pessoa` que representa uma pessoa com nome, idade e uma lista de animais adotados. A classe `Main` é a classe principal onde criamos objetos das outras classes e executamos métodos para demonstrar o funcionamento.

Ao executar o código, o resultado será:

```
Nome: João
Idade: 25
Animais adotados:
Leão@6d06d69c
O animal Leão está fazendo barulho!
```

Observe que utilizamos a anotação `@Override` para indicar que estamos sobrescrevendo o método `toString()` da classe `Object` na classe `Animal`. Isso nos permite exibir informações personalizadas do objeto ao invés do valor padrão retornado por `toString()`.