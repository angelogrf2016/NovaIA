Aqui está um exemplo de código complexo em JavaScript que envolve o uso de funções, loops e condicionais:

```javascript
// Função para calcular o fatorial de um número
function calcularFatorial(numero) {
  let fatorial = 1;
  
  for (let i = 1; i <= numero; i++) {
    fatorial *= i;
  }
  
  return fatorial;
}

// Função para verificar se um número é primo
function isPrimo(numero) {
  if (numero <= 1) {
    return false;
  }
  
  for (let i = 2; i < numero; i++) {
    if (numero % i === 0) {
      return false;
    }
  }
  
  return true;
}

// Função para encontrar os números primos até um determinado limite
function encontrarPrimosAte(limite) {
  const primos = [];
  
  for (let i = 2; i <= limite; i++) {
    if (isPrimo(i)) {
      primos.push(i);
    }
  }
  
  return primos;
}

// Função para calcular a soma de todos os fatoriais dos números primos até um determinado limite
function calcularSomaFatoriaisPrimosAte(limite) {
  const primos = encontrarPrimosAte(limite);
  let somaFatoriais = 0;
  
  for (let i = 0; i < primos.length; i++) {
    somaFatoriais += calcularFatorial(primos[i]);
  }
  
  return somaFatoriais;
}

// Exemplo de uso das funções
const limite = 10;
const somaFatoriaisPrimos = calcularSomaFatoriaisPrimosAte(limite);
console.log(`A soma dos fatoriais dos números primos até ${limite} é: ${somaFatoriaisPrimos}`);
```

Neste exemplo, criamos algumas funções para calcular o fatorial de um número, verificar se um número é primo, encontrar os números primos até um determinado limite e calcular a soma de todos os fatoriais dos números primos até um determinado limite.

Em seguida, utilizamos essas funções para calcular a soma dos fatoriais dos números primos até o limite especificado (neste caso, 10) e exibimos o resultado no console.

Esse código é apenas um exemplo de como podemos utilizar funções, loops e condicionais em JavaScript para resolver um problema mais complexo.