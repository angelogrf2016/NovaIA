Claro! Aqui está um exemplo de código complexo em C++ que envolve manipulação de strings, estruturas de controle e funções:

```cpp
#include <iostream>
#include <string>
#include <cmath>

struct Pessoa {
    std::string nome;
    int idade;
};

void imprimirPessoa(const Pessoa& pessoa) {
    std::cout << "Nome: " << pessoa.nome << std::endl;
    std::cout << "Idade: " << pessoa.idade << std::endl;
}

int calcularQuadrado(int numero) {
    return std::pow(numero, 2);
}

int main() {
    std::string nome;
    int idade;

    std::cout << "Digite o nome: ";
    std::getline(std::cin, nome);

    std::cout << "Digite a idade: ";
    std::cin >> idade;

    Pessoa pessoa;
    pessoa.nome = nome;
    pessoa.idade = idade;

    imprimirPessoa(pessoa);

    int numero;
    std::cout << "Digite um número: ";
    std::cin >> numero;

    int quadrado = calcularQuadrado(numero);
    std::cout << "O quadrado de " << numero << " é " << quadrado << std::endl;

    return 0;
}
```

Neste código, criamos uma estrutura chamada `Pessoa` que tem um campo para o nome e outro para a idade. Em seguida, temos uma função `imprimirPessoa` que recebe uma pessoa como parâmetro e imprime as informações na tela.

No `main`, pedimos ao usuário para digitar o nome e a idade, e em seguida, criamos uma pessoa com essas informações e a imprimimos na tela.

Depois, pedimos ao usuário para digitar um número e calculamos o quadrado desse número usando a função `calcularQuadrado`. Por fim, imprimimos o resultado na tela.

Espero que este exemplo atenda às suas expectativas!