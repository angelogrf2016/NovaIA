Aqui está um exemplo de código SQL complexo que envolve várias tabelas e operações complexas:

```sql
SELECT c.nome_cliente, p.nome_produto, SUM(iv.quantidade * iv.preco_unitario) AS total_vendas
FROM clientes c
JOIN pedidos p ON c.id_cliente = p.id_cliente
JOIN itens_venda iv ON p.id_pedido = iv.id_pedido
GROUP BY c.nome_cliente, p.nome_produto
HAVING total_vendas > 1000
ORDER BY total_vendas DESC;
```

Neste exemplo, estamos selecionando o nome do cliente, o nome do produto e o total de vendas para cada cliente e produto. Estamos usando as tabelas `clientes`, `pedidos` e `itens_venda`. Fazemos uma junção entre essas tabelas usando as chaves primárias e estrangeiras correspondentes. Em seguida, agrupamos os resultados pelo nome do cliente e do produto e calculamos a soma das quantidades multiplicadas pelo preço unitário para obter o total de vendas. 

Usamos a cláusula `HAVING` para filtrar apenas os resultados cujo total de vendas seja superior a 1000. Por fim, ordenamos os resultados em ordem decrescente pelo total de vendas. É importante notar que este é apenas um exemplo fictício e as tabelas e colunas utilizadas podem variar dependendo do seu banco de dados.