Aqui está um exemplo de código complexo em JavaScript que cria uma calculadora com operações básicas:

```javascript
// Função para adicionar dois números
function adicionar(a, b) {
  return a + b;
}

// Função para subtrair dois números
function subtrair(a, b) {
  return a - b;
}

// Função para multiplicar dois números
function multiplicar(a, b) {
  return a * b;
}

// Função para dividir dois números
function dividir(a, b) {
  if (b === 0) {
    return "Erro: Divisão por zero!";
  }
  return a / b;
}

// Função principal que executa a calculadora
function executarCalculadora() {
  let numero1 = parseFloat(prompt("Digite o primeiro número:"));
  let numero2 = parseFloat(prompt("Digite o segundo número:"));
  
  let operacao = prompt("Digite a operação desejada (+, -, *, /):");
  
  let resultado;
  
  switch (operacao) {
    case "+":
      resultado = adicionar(numero1, numero2);
      break;
    case "-":
      resultado = subtrair(numero1, numero2);
      break;
    case "*":
      resultado = multiplicar(numero1, numero2);
      break;
    case "/":
      resultado = dividir(numero1, numero2);
      break;
    default:
      resultado = "Erro: Operação inválida!";
      break;
  }
  
  console.log("Resultado:", resultado);
}

// Chamar a função principal
executarCalculadora();
```

Este código cria uma calculadora simples que solicita ao usuário dois números e a operação desejada (+, -, *, /). Em seguida, a calculadora executa a operação e exibe o resultado no console.