Aqui está um exemplo de código complexo em ADA que simula um sistema de controle de tráfego:

```ada
with Ada.Text_IO; use Ada.Text_IO;
with Ada.Integer_Text_IO; use Ada.Integer_Text_IO;
with Ada.Calendar; use Ada.Calendar;

procedure Traffic_Control is

   type Vehicle_Type is (Car, Motorcycle, Bus, Truck);
   
   type Vehicle is record
      Type : Vehicle_Type;
      Speed : Integer;
   end record;
   
   type Vehicle_Arrival is record
      Time : Time;
      Vehicle : Vehicle;
   end record;
   
   type Vehicle_Arrival_List is array (Positive range <>) of Vehicle_Arrival;
   
   procedure Sort_Vehicle_Arrivals (List : in out Vehicle_Arrival_List) is
      Temp : Vehicle_Arrival;
   begin
      for I in List'First+1 .. List'Last loop
         Temp := List(I);
         for J in reverse I-1 .. List'First loop
            if List(J).Time > Temp.Time then
               List(J+1) := List(J);
               List(J) := Temp;
            else
               exit;
            end if;
         end loop;
      end loop;
   end Sort_Vehicle_Arrivals;
   
   procedure Print_Vehicle_Arrivals (List : in Vehicle_Arrival_List) is
   begin
      for I in List'Range loop
         Put("Time: ");
         Put(Time'Image(List(I).Time));
         Put(" | Type: ");
         case List(I).Vehicle.Type is
            when Car =>
               Put("Car");
            when Motorcycle =>
               Put("Motorcycle");
            when Bus =>
               Put("Bus");
            when Truck =>
               Put("Truck");
         end case;
         Put(" | Speed: ");
         Put(List(I).Vehicle.Speed, Width => 3);
         New_Line;
      end loop;
   end Print_Vehicle_Arrivals;
   
   procedure Simulate_Traffic_Control is
      Arrivals : Vehicle_Arrival_List(1..10);
   begin
      -- Simulate vehicle arrivals
      for I in Arrivals'Range loop
         Arrivals(I).Time := Clock;
         Arrivals(I).Vehicle.Type := Vehicle_Type(I mod 4);
         Arrivals(I).Vehicle.Speed := I * 10;
         delay 0.1;
      end loop;
      
      -- Sort vehicle arrivals by time
      Sort_Vehicle_Arrivals(Arrivals);
      
      -- Print vehicle arrivals
      Print_Vehicle_Arrivals(Arrivals);
   end Simulate_Traffic_Control;
   
begin
   Simulate_Traffic_Control;
end Traffic_Control;
```

Este código define um tipo de veículo (`Vehicle_Type`) com os valores `Car`, `Motorcycle`, `Bus` e `Truck`. Em seguida, define um registro de veículo (`Vehicle`) com campos para o tipo de veículo e velocidade.

O código também define um registro de chegada de veículo (`Vehicle_Arrival`) com campos para a hora da chegada e para um veículo. 

Há, também, um tipo de dado chamado `Vehicle_Arrival_List`, que é uma matriz de `Vehicle_Arrival`.

O procedimento `Sort_Vehicle_Arrivals` é utilizado para ordenar a lista de chegadas de veículos por tempo de chegada. O procedimento `Print_Vehicle_Arrivals` é utilizado para imprimir a lista de chegadas de veículos.

O procedimento principal `Simulate_Traffic_Control` simula a chegada de veículos, armazena-os em uma lista, ordena a lista e, em seguida, imprime a lista ordenada.

Finalmente, o programa principal chama o procedimento `Simulate_Traffic_Control` para executar a simulação.

Este código é um exemplo simples e pode ser expandido para incluir mais funcionalidades, como cálculos de velocidade média ou determinar o tempo mínimo necessário para atravessar um cruzamento com base na velocidade e tipo de veículo.