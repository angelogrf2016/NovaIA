Aqui está um exemplo de código complexo em Assembly para a arquitetura x86:

section .data
    msg db 'Ola, mundo!',0

section .text
    global _start

_start:
    ; chamada do sistema para imprimir a mensagem
    mov eax, 4
    mov ebx, 1
    mov ecx, msg
    mov edx, 13
    int 0x80

    ; chamada do sistema para sair do programa
    mov eax, 1
    xor ebx, ebx
    int 0x80

Esse código utiliza a chamada do sistema int 0x80 para imprimir a mensagem "Ola, mundo!" na saída padrão e, em seguida, encerra o programa. Ele é escrito para a plataforma Linux e utiliza a sintaxe do NASM (Netwide Assembler).