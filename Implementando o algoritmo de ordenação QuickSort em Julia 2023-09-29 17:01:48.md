Claro! Aqui está um código em Julia que implementa o algoritmo de ordenação QuickSort:

```julia
function quicksort(arr, low, high)
    if low < high
        pivot = partition(arr, low, high)
        quicksort(arr, low, pivot-1)
        quicksort(arr, pivot+1, high)
    end
end

function partition(arr, low, high)
    pivot = arr[high]
    i = low - 1
    
    for j = low:high-1
        if arr[j] <= pivot
            i += 1
            arr[i], arr[j] = arr[j], arr[i]
        end
    end
    
    arr[i+1], arr[high] = arr[high], arr[i+1]
    return i+1
end

arr = [10, 7, 8, 9, 1, 5]
n = length(arr)
quicksort(arr, 1, n)

println("Array ordenado: ", arr)
```

Este código implementa a função `quicksort` que recebe um array, o índice `low` (inicial) e o índice `high` (final). A função `quicksort` chama a função `partition` para dividir o array em duas partes e chama recursivamente o `quicksort` para ordenar cada parte.

A função `partition` escolhe um elemento como pivô e coloca todos os elementos menores que o pivô à sua esquerda e todos os elementos maiores à sua direita. Ela retorna o índice do pivô.

No final, o código ordena o array `[10, 7, 8, 9, 1, 5]` usando o `quicksort` e imprime o array ordenado.