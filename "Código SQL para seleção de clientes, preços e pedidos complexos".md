Claro! Aqui está um exemplo de código SQL que envolve várias tabelas e operações complexas:

```
SELECT c.nome, p.preco
FROM cliente c
JOIN pedido pe ON c.id_cliente = pe.id_cliente
JOIN produto p ON pe.id_produto = p.id_produto
WHERE pe.data_pedido >= '2021-01-01'
GROUP BY c.nome
HAVING SUM(p.preco) > (
    SELECT AVG(preco)
    FROM produto
)
ORDER BY c.nome ASC;
```

Neste exemplo, estamos selecionando o nome dos clientes e o preço dos produtos que eles pediram em um determinado período de tempo. Estamos fazendo isso ao juntar as tabelas `cliente`, `pedido` e `produto` usando as colunas `id_cliente` e `id_produto` como chaves estrangeiras.

Estamos também filtrando os resultados para incluir apenas os pedidos feitos a partir de 1 de janeiro de 2021. Em seguida, estamos agrupando os resultados pelo nome do cliente e restringindo o resultado para incluir apenas os clientes cuja soma dos preços dos produtos pedidos seja maior do que a média do preço de todos os produtos. Finalmente, estamos ordenando os resultados em ordem alfabética pelo nome do cliente.

Espero que este exemplo tenha atendido às suas expectativas. Lembre-se de adaptar o código às suas tabelas e estrutura de dados específicos.