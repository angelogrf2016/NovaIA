Aqui está um exemplo de código complexo em Kotlin:

```kotlin
data class Person(val name: String, val age: Int)

fun main() {
    val people = listOf(
        Person("John", 25),
        Person("Jane", 30),
        Person("Mike", 20)
    )

    val filteredPeople = people.filter { it.age > 21 }
    val oldestPerson = filteredPeople.maxByOrNull { it.age }

    println("Oldest person: ${oldestPerson?.name}")
}
```

Neste exemplo, temos uma classe `Person` que possui propriedades `name` e `age`. Em seguida, no método `main`, criamos uma lista de pessoas e filtramos apenas aquelas com idade superior a 21 anos. Depois, encontramos a pessoa mais velha utilizando a função `maxByOrNull`. Por fim, imprimimos o nome da pessoa mais velha.

Este código demonstra o uso de classes de dados, funções de ordem superior (`filter` e `maxByOrNull`) e o uso do operador `?.` para evitar possíveis `NullPointerExceptions`.