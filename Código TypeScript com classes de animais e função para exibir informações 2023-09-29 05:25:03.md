Claro! Aqui está um exemplo de código complexo em TypeScript:

```typescript
// Classe base para um animal
class Animal {
  private nome: string;
  private idade: number;

  constructor(nome: string, idade: number) {
    this.nome = nome;
    this.idade = idade;
  }

  public getNome(): string {
    return this.nome;
  }

  public getIdade(): number {
    return this.idade;
  }

  public fazerBarulho(): void {
    console.log("O animal está fazendo barulho...");
  }
}

// Classe derivada de Animal para representar um gato
class Gato extends Animal {
  private cor: string;

  constructor(nome: string, idade: number, cor: string) {
    super(nome, idade);
    this.cor = cor;
  }

  public getCor(): string {
    return this.cor;
  }

  public fazerBarulho(): void {
    console.log("O gato está miando...");
  }
}

// Classe derivada de Animal para representar um cachorro
class Cachorro extends Animal {
  private raca: string;

  constructor(nome: string, idade: number, raca: string) {
    super(nome, idade);
    this.raca = raca;
  }

  public getRaca(): string {
    return this.raca;
  }

  public fazerBarulho(): void {
    console.log("O cachorro está latindo...");
  }
}

// Função que recebe um array de animais e exibe informações sobre eles
function exibirInformacoesAnimais(animais: Animal[]): void {
  for (let animal of animais) {
    console.log(`Nome: ${animal.getNome()}`);
    console.log(`Idade: ${animal.getIdade()}`);
    animal.fazerBarulho();

    if (animal instanceof Gato) {
      console.log(`Cor do gato: ${(animal as Gato).getCor()}`);
    } else if (animal instanceof Cachorro) {
      console.log(`Raça do cachorro: ${(animal as Cachorro).getRaca()}`);
    }

    console.log("-----");
  }
}

// Criando instâncias de animais
const gato1 = new Gato("Miau", 3, "Amarelo");
const cachorro1 = new Cachorro("Rex", 5, "Labrador");

// Criando um array de animais e chamando a função para exibir as informações
const animais = [gato1, cachorro1];
exibirInformacoesAnimais(animais);
```

Neste exemplo, temos uma classe base `Animal` e duas classes derivadas `Gato` e `Cachorro`. Cada uma dessas classes possui propriedades e métodos específicos, com a classe base contendo os atributos comuns a todos os animais. Também temos uma função `exibirInformacoesAnimais` que recebe um array de animais e exibe informações sobre cada um deles. Por fim, criamos instâncias de animais e chamamos a função para exibir as informações.