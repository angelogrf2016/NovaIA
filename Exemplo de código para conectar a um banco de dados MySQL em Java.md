Claro! Aqui está um exemplo de código para conectar a um banco de dados MySQL em Java:

```java
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConexaoMySQL {
    private static final String URL = "jdbc:mysql://localhost:3306/nome_banco_de_dados";
    private static final String USUARIO = "root";
    private static final String SENHA = "senha";

    public static Connection obterConexao() {
        Connection conexao = null;

        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            conexao = DriverManager.getConnection(URL, USUARIO, SENHA);
            System.out.println("Conexão estabelecida com sucesso!");
        } catch (ClassNotFoundException e) {
            System.out.println("Driver do MySQL não encontrado!");
        } catch (SQLException e) {
            System.out.println("Erro ao conectar ao banco de dados!");
            e.printStackTrace();
        }

        return conexao;
    }

    public static void fecharConexao(Connection conexao) {
        if (conexao != null) {
            try {
                conexao.close();
                System.out.println("Conexão fechada com sucesso!");
            } catch (SQLException e) {
                System.out.println("Erro ao fechar a conexão!");
                e.printStackTrace();
            }
        }
    }
}
```

Neste exemplo, a classe `ConexaoMySQL` possui dois métodos estáticos. O método `obterConexao()` retorna uma instância da `Connection` para o banco de dados MySQL, enquanto o método `fecharConexao()` fecha a conexão com o banco de dados.

Para utilizar essa classe, basta chamar `ConexaoMySQL.obterConexao()` para obter a conexão com o banco de dados e `ConexaoMySQL.fecharConexao(conexao)` para fechar a conexão quando não for mais necessária.