Claro, aqui está um exemplo de um código complexo em Dart. Este código cria uma classe chamada "Person" que tem as propriedades "name" e "age", e também tem os métodos "greet" e "getYearOfBirth".

```dart
class Person {
  String name;
  int age;
  
  Person(this.name, this.age);
  
  void greet() {
    print("Olá, meu nome é $name e eu tenho $age anos.");
  }
  
  int getYearOfBirth() {
    return DateTime.now().year - age;
  }
}

void main() {
  Person person = Person("João", 30);
  person.greet();
  
  int yearOfBirth = person.getYearOfBirth();
  print("Ano de nascimento: $yearOfBirth");
}
```

Neste código, a classe "Person" tem um construtor que recebe o nome e a idade da pessoa e atribui esses valores às propriedades correspondentes. A classe também tem dois métodos: "greet" que imprime uma mensagem de saudação com o nome e a idade da pessoa, e "getYearOfBirth" que retorna o ano de nascimento baseado na idade atual.

No método "main", um objeto "Person" é criado com o nome "João" e idade 30. Em seguida, o método "greet" é chamado para imprimir a mensagem de saudação. Por fim, o método "getYearOfBirth" é chamado e o ano de nascimento é impresso.