Aqui está um exemplo de código complexo em Rust que usa estruturas e algoritmos avançados:

```rust
use std::collections::{BinaryHeap, HashMap};
use std::cmp::Reverse;

#[derive(Debug, Eq, PartialEq, Copy, Clone)]
struct Node {
    id: usize,
    distance: u32,
}

impl Ord for Node {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        self.distance.cmp(&other.distance)
    }
}

impl PartialOrd for Node {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        Some(self.cmp(other))
    }
}

fn dijkstra(graph: &[Vec<(usize, u32)>], start: usize, end: usize) -> Option<u32> {
    let mut distances: HashMap<usize, u32> = HashMap::new();
    let mut heap = BinaryHeap::new();

    heap.push(Reverse(Node {
        id: start,
        distance: 0,
    }));

    while let Some(Reverse(Node { id, distance })) = heap.pop() {
        if id == end {
            return Some(distance);
        }

        if let Some(&prev_distance) = distances.get(&id) {
            if distance >= prev_distance {
                continue;
            }
        }

        distances.insert(id, distance);

        for &(neighbour, weight) in &graph[id] {
            heap.push(Reverse(Node {
                id: neighbour,
                distance: distance + weight,
            }));
        }
    }

    None
}

fn main() {
    let graph = vec![
        vec![(1, 4), (2, 2)],
        vec![(3, 5)],
        vec![(1, 1), (3, 8)],
        vec![(4, 3)],
        vec![],
    ];

    let start = 0;
    let end = 4;
    
    if let Some(distance) = dijkstra(&graph, start, end) {
        println!("Shortest distance from {} to {} is {}", start, end, distance);
    } else {
        println!("No path found from {} to {}", start, end);
    }
}
```

Este código implementa o algoritmo de Dijkstra para encontrar o caminho mais curto em um grafo ponderado. A função `dijkstra` recebe o grafo como uma lista de adjacências, o vértice de partida e o vértice de chegada. Se um caminho é encontrado, ele retorna a distância mínima entre os vértices. Caso contrário, retorna `None`.

O código usa uma fila de prioridade (`BinaryHeap`) para manter os nós a serem explorados, ordenados por distância mínima. A cada iteração, o nó com a menor distância é removido da fila e suas arestas são exploradas. Se a distância para um vizinho for menor do que a encontrada anteriormente, o vizinho é adicionado à fila com a nova distância.

O exemplo de grafo usado no código é o seguinte:

```
0 -> 1 (peso 4) -> 3 (peso 3) -> 4
|    | (peso 2)    | (peso 8)
v    v             v
2 -> 3 (peso 5)
```

O código imprime o caminho mais curto entre os vértices 0 e 4, que é 9.