Aqui está um exemplo de código complexo em Perl que lê um arquivo de texto, manipula os dados e gera um relatório:

```perl
#!/usr/bin/perl

use strict;
use warnings;

# Abrir o arquivo de entrada para leitura
open my $input_file, '<', 'dados.txt' or die "Não foi possível abrir o arquivo de entrada: $!";

# Variáveis para armazenar os dados
my %contagem;
my $total_linhas = 0;
my $total_palavras = 0;
my $total_caracteres = 0;

# Ler o arquivo linha por linha
while (my $linha = <$input_file>) {
    # Incrementar o total de linhas
    $total_linhas++;

    # Remover caracteres indesejados
    $linha =~ s/[^a-zA-Z0-9\s]//g;

    # Dividir a linha em palavras
    my @palavras = split /\s+/, $linha;

    # Incrementar a contagem de palavras
    $total_palavras += scalar @palavras;

    # Incrementar a contagem de caracteres
    $total_caracteres += length($linha);

    # Incrementar a contagem de ocorrências de cada palavra
    foreach my $palavra (@palavras) {
        $contagem{$palavra}++;
    }
}

# Fechar o arquivo de entrada
close $input_file;

# Abrir o arquivo de saída para escrita
open my $output_file, '>', 'relatorio.txt' or die "Não foi possível abrir o arquivo de saída: $!";

# Escrever o relatório
print $output_file "========== RELATÓRIO ==========\n";
print $output_file "Total de linhas: $total_linhas\n";
print $output_file "Total de palavras: $total_palavras\n";
print $output_file "Total de caracteres: $total_caracteres\n\n";

print $output_file "========== CONTAGEM DE PALAVRAS ==========\n";
foreach my $palavra (sort keys %contagem) {
    print $output_file "$palavra: $contagem{$palavra}\n";
}

# Fechar o arquivo de saída
close $output_file;

print "Relatório gerado com sucesso!\n";
```

Este código lê um arquivo chamado "dados.txt" e realiza as seguintes operações:

1. Abre o arquivo de entrada para leitura.
2. Inicializa algumas variáveis para armazenar os dados.
3. Lê o arquivo linha por linha.
4. Remove caracteres indesejados e divide cada linha em palavras.
5. Incrementa o total de linhas, o total de palavras e o total de caracteres.
6. Incrementa a contagem de ocorrências de cada palavra.
7. Fecha o arquivo de entrada.
8. Abre um arquivo chamado "relatorio.txt" para escrita.
9. Escreve um relatório com o total de linhas, o total de palavras e o total de caracteres.
10. Escreve a contagem de ocorrências de cada palavra.
11. Fecha o arquivo de saída.
12. Imprime uma mensagem de sucesso.

Você precisará criar um arquivo de entrada chamado "dados.txt" com os dados que deseja processar. O relatório será gerado no arquivo "relatorio.txt". Certifique-se de ter permissão para ler e escrever nesses arquivos.