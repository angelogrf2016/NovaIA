O código abaixo é um exemplo de um código complexo em Scala que implementa uma solução para o problema do caixeiro viajante usando o algoritmo genético:

```scala
import scala.util.Random

class City(val x: Int, val y: Int)

object TSPGeneticAlgorithm {
  val numberOfCities = 10
  val populationSize = 100
  val maxGenerations = 1000
  val mutationRate = 0.01

  val random = new Random()

  def main(args: Array[String]): Unit = {
    val cities = generateCities(numberOfCities)
    var population = generateInitialPopulation(cities, populationSize)

    var generation = 1
    var bestDistance = Double.MaxValue
    var bestPath = List.empty[City]

    while (generation <= maxGenerations && bestDistance > 0) {
      population = evolvePopulation(population)
      val currentBestPath = population(0)
      val currentBestDistance = calculateDistance(currentBestPath, cities)

      if (currentBestDistance < bestDistance) {
        bestDistance = currentBestDistance
        bestPath = currentBestPath
      }

      println(s"Generation $generation: Best Distance = $bestDistance")
      generation += 1
    }

    println(s"Best Path: ${bestPath.map(c => s"(${c.x}, ${c.y})").mkString(" -> ")}")
    println(s"Best Distance: $bestDistance")
  }

  def generateCities(number: Int): List[City] = {
    (0 until number).map(_ => new City(random.nextInt(100), random.nextInt(100))).toList
  }

  def generateInitialPopulation(cities: List[City], size: Int): List[List[City]] = {
    (0 until size).map(_ => random.shuffle(cities)).toList
  }

  def calculateDistance(path: List[City], cities: List[City]): Double = {
    val distances = path.sliding(2).map {
      case List(city1, city2) => calculateDistanceBetweenCities(city1, city2)
    }.toList

    distances.sum + calculateDistanceBetweenCities(path.last, path.head)
  }

  def calculateDistanceBetweenCities(city1: City, city2: City): Double = {
    math.sqrt(math.pow(city2.x - city1.x, 2) + math.pow(city2.y - city1.y, 2))
  }

  def evolvePopulation(population: List[List[City]]): List[List[City]] = {
    val eliteSize = populationSize / 10
    val elite = population.take(eliteSize)
    val offspring = crossoverPopulation(elite)

    elite ::: mutatePopulation(offspring)
  }

  def crossoverPopulation(elite: List[List[City]]): List[List[City]] = {
    val offspring = elite.map { parent1 =>
      val parent2 = elite(random.nextInt(elite.size))
      val crossoverPoint = random.nextInt(numberOfCities)

      val child1 = parent1.take(crossoverPoint) ++ parent2.drop(crossoverPoint)
      val child2 = parent2.take(crossoverPoint) ++ parent1.drop(crossoverPoint)

      List(child1, child2)
    }

    offspring.flatten
  }

  def mutatePopulation(population: List[List[City]]): List[List[City]] = {
    population.map { path =>
      path.map { city =>
        if (random.nextDouble() < mutationRate) {
          val randomIndex = random.nextInt(numberOfCities)
          path.updated(randomIndex, city)
        } else {
          city
        }
      }
    }
  }
}
```

Neste código, implementamos uma solução para o problema do caixeiro viajante usando o algoritmo genético. O algoritmo começa gerando um conjunto de cidades aleatórias, representadas pela classe `City`. Em seguida, é gerada uma população inicial de rotas, onde cada rota é uma permutação aleatória das cidades.

O algoritmo então evolui a população através de gerações. Em cada geração, a população é cruzada e mutada, gerando uma nova geração de rotas. O processo de cruzamento é feito selecionando dois pais da população elite, escolhidos aleatoriamente, e realizando um ponto de cruzamento para criar dois filhos. O processo de mutação é feito trocando aleatoriamente duas cidades em uma rota, com uma taxa de mutação definida.

O algoritmo continua evoluindo a população por um número máximo de gerações ou até que a distância da melhor rota seja igual a zero. A cada geração, o melhor caminho e a sua distância são impressos na saída.

Ao final das gerações, o algoritmo imprime o melhor caminho encontrado e a sua distância.