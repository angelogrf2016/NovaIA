Para criar um código complexo em PROLOG, vamos criar um programa de recomendação de filmes com base nos gostos do usuário. O programa irá armazenar informações sobre os filmes e as preferências dos usuários, e irá sugerir filmes com base em gostos semelhantes.

```prolog
% Base de conhecimento de filmes
filme(avengers_endgame, acao, 2019, 8.4).
filme(joker, drama, 2019, 8.5).
filme(interstellar, ficcao, 2014, 8.6).
filme(the_shawshank_redemption, drama, 1994, 9.3).
filme(the_dark_knight, acao, 2008, 9.0).
filme(inception, ficcao, 2010, 8.7).
filme(pulp_fiction, crime, 1994, 8.9).
filme(fight_club, drama, 1999, 8.8).
filme(the_matrix, ficcao, 1999, 8.7).
filme(forrest_gump, drama, 1994, 8.8).

% Base de conhecimento de preferências do usuário
gosta(joao, acao).
gosta(joao, ficcao).
gosta(joao, drama).

gosta(maria, drama).
gosta(maria, crime).

gosta(pedro, drama).
gosta(pedro, ficcao).

% Predicado para sugerir filmes
sugerir_filme(Pessoa, Filme) :-
    gosta(Pessoa, Genero),
    filme(Filme, Genero, _, _).

% Predicado para classificar os filmes por rating
classificar_filmes(Pessoa, Filmes) :-
    findall(Filme, sugerir_filme(Pessoa, Filme), FilmesSugeridos),
    sort(FilmesSugeridos, Filmes).

% Predicado para encontrar o filme com o maior rating
melhor_filme(Pessoa, Filme) :-
    classificar_filmes(Pessoa, Filmes),
    reverse(Filmes, [Filme|_]).

% Exemplo de consulta
?- melhor_filme(joao, Filme).
Filme = the_dark_knight.
```

Neste exemplo, temos uma base de conhecimento de filmes, contendo informações sobre o nome, gênero, ano de lançamento e classificação de cada filme. Também temos uma base de conhecimento de preferências do usuário, onde cada usuário possui uma lista de gêneros de filmes que ele gosta.

O predicado `sugerir_filme/2` recebe uma pessoa como entrada e sugere um filme que tenha um gênero que a pessoa gosta.

O predicado `classificar_filmes/2` classifica os filmes sugeridos para uma pessoa em ordem crescente de rating.

O predicado `melhor_filme/2` encontra o filme com o maior rating entre os filmes sugeridos para uma pessoa.

Na consulta de exemplo, estamos encontrando o melhor filme sugerido para a pessoa "joao", que é "the_dark_knight" (Batman: O Cavaleiro das Trevas), com uma classificação de 9.0.