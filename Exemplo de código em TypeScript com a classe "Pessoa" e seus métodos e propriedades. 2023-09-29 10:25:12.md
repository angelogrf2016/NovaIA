Claro! Aqui está um exemplo de código em TypeScript que cria uma classe chamada "Pessoa" com propriedades e métodos complexos:

```typescript
class Pessoa {
  nome: string;
  idade: number;
  amigos: string[];
  
  constructor(nome: string, idade: number, amigos: string[]) {
    this.nome = nome;
    this.idade = idade;
    this.amigos = amigos;
  }
  
  saudacao(): string {
    return `Olá, meu nome é ${this.nome} e tenho ${this.idade} anos.`;
  }
  
  adicionarAmigo(amigo: string): void {
    this.amigos.push(amigo);
  }
  
  listarAmigos(): void {
    console.log("Meus amigos são:");
    this.amigos.forEach(amigo => {
      console.log(amigo);
    });
  }
}

const pessoa1 = new Pessoa("João", 25, ["Maria", "Pedro"]);
console.log(pessoa1.saudacao()); // Saída: Olá, meu nome é João e tenho 25 anos.

pessoa1.adicionarAmigo("Ana");
pessoa1.listarAmigos(); // Saída: Meus amigos são: Maria, Pedro, Ana
```

Neste exemplo, a classe "Pessoa" possui três propriedades: "nome", "idade" e "amigos". O construtor da classe é usado para definir os valores iniciais dessas propriedades. A classe também possui três métodos: "saudacao", que retorna uma saudação com o nome e a idade da pessoa, "adicionarAmigo", que adiciona um novo amigo à lista de amigos, e "listarAmigos", que exibe a lista de amigos no console. No final, um objeto "pessoa1" é criado e os métodos são chamados para verificar a saudação e a lista de amigos atualizada.