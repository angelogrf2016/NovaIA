Aqui está um exemplo de código complexo em PERL:

```perl
#!/usr/bin/perl

use strict;
use warnings;

# Função recursiva para calcular o fatorial de um número
sub fatorial {
    my $n = shift;
    return 1 if $n == 0;
    return $n * fatorial($n - 1);
}

# Função para calcular o número de Fibonacci
sub fibonacci {
    my $n = shift;
    return $n if $n <= 1;
    return fibonacci($n - 1) + fibonacci($n - 2);
}

# Função para verificar se um número é primo
sub is_prime {
    my $n = shift;
    return 0 if $n < 2;
    for (my $i = 2; $i <= sqrt($n); $i++) {
        return 0 if $n % $i == 0;
    }
    return 1;
}

# Função para calcular o quadrado de um número
sub square {
    my $n = shift;
    return $n * $n;
}

# Função para calcular o cubo de um número
sub cube {
    my $n = shift;
    return $n * $n * $n;
}

# Exemplos de uso das funções
print "O fatorial de 5 é: " . fatorial(5) . "\n";
print "O 7º número de Fibonacci é: " . fibonacci(7) . "\n";
print "O número 13 é primo? " . (is_prime(13) ? "Sim" : "Não") . "\n";
print "O quadrado de 4 é: " . square(4) . "\n";
print "O cubo de 3 é: " . cube(3) . "\n";
```

Neste exemplo, temos cinco funções diferentes: `fatorial` para calcular o fatorial de um número, `fibonacci` para calcular o número de Fibonacci, `is_prime` para verificar se um número é primo, `square` para calcular o quadrado de um número e `cube` para calcular o cubo de um número.

Em seguida, temos exemplos de uso dessas funções para calcular o fatorial de 5, o 7º número de Fibonacci, verificar se o número 13 é primo, calcular o quadrado de 4 e o cubo de 3. Os resultados são impressos na saída padrão.