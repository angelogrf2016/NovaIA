Aqui está um exemplo de código complexo em JavaScript que envolve manipulação de dados, estruturas de controle e funções:

```javascript
// Definindo uma classe para representar uma pessoa
class Pessoa {
  constructor(nome, idade) {
    this.nome = nome;
    this.idade = idade;
  }

  // Método para verificar se a pessoa é maior de idade
  isMaiorDeIdade() {
    return this.idade >= 18;
  }
}

// Função para criar uma lista de pessoas
function criarListaPessoas() {
  const listaPessoas = [];

  // Loop para adicionar pessoas à lista
  for (let i = 0; i < 5; i++) {
    const nome = prompt("Digite o nome da pessoa:");
    const idade = parseInt(prompt("Digite a idade da pessoa:"));

    const pessoa = new Pessoa(nome, idade);
    listaPessoas.push(pessoa);
  }

  return listaPessoas;
}

// Função para filtrar as pessoas maiores de idade da lista
function filtrarMaioresDeIdade(listaPessoas) {
  const listaMaioresDeIdade = [];

  // Loop para verificar se cada pessoa é maior de idade
  for (let i = 0; i < listaPessoas.length; i++) {
    const pessoa = listaPessoas[i];

    if (pessoa.isMaiorDeIdade()) {
      listaMaioresDeIdade.push(pessoa);
    }
  }

  return listaMaioresDeIdade;
}

// Função para imprimir a lista de pessoas
function imprimirListaPessoas(listaPessoas) {
  // Loop para imprimir as informações de cada pessoa
  for (let i = 0; i < listaPessoas.length; i++) {
    const pessoa = listaPessoas[i];
    console.log(`Nome: ${pessoa.nome}, Idade: ${pessoa.idade}`);
  }
}

// Criar lista de pessoas
const listaPessoas = criarListaPessoas();

// Filtrar pessoas maiores de idade
const listaMaioresDeIdade = filtrarMaioresDeIdade(listaPessoas);

// Imprimir lista de pessoas
console.log("Lista de Pessoas:");
imprimirListaPessoas(listaPessoas);

// Imprimir lista de pessoas maiores de idade
console.log("Lista de Pessoas Maiores de Idade:");
imprimirListaPessoas(listaMaioresDeIdade);
```

Neste código, temos uma classe chamada `Pessoa` que representa uma pessoa com nome e idade. A classe possui um método `isMaiorDeIdade()` que verifica se a pessoa é maior de idade com base na idade fornecida.

Em seguida, temos a função `criarListaPessoas()` que cria uma lista de pessoas, solicitando ao usuário que digite o nome e a idade de cada pessoa.

A função `filtrarMaioresDeIdade()` recebe a lista de pessoas e retorna uma nova lista contendo apenas as pessoas maiores de idade.

Por fim, a função `imprimirListaPessoas()` é usada para imprimir as informações de cada pessoa na lista.

No final do código, criamos uma lista de pessoas, filtramos apenas as pessoas maiores de idade e imprimimos as listas de pessoas e pessoas maiores de idade no console.