Aqui está um exemplo de código complexo em Perl que analisa um arquivo de log e gera estatísticas sobre as solicitações HTTP feitas ao servidor:

```
#!/usr/bin/perl

use strict;
use warnings;

# Define o arquivo de log
my $log_file = 'access.log';

# Abre o arquivo de log
open my $log_fh, '<', $log_file or die "Não foi possível abrir o arquivo de log: $!";

# Inicializa as variáveis de contagem
my $total_requests = 0;
my %requests_by_status;
my %requests_by_ip;

# Loop através de cada linha do arquivo de log
while (my $line = <$log_fh>) {
    chomp $line;
    $total_requests++;

    # Extrai o endereço IP e o código de status de cada linha
    my ($ip, $status) = ($line =~ /^(\d+\.\d+\.\d+\.\d+) - - .+? (\d+) .+$/);

    # Incrementa a contagem de solicitações por código de status
    $requests_by_status{$status}++;

    # Incrementa a contagem de solicitações por endereço IP
    $requests_by_ip{$ip}++;
}

# Fecha o arquivo de log
close $log_fh;

# Imprime as estatísticas
print "Estatísticas de solicitações HTTP:\n";
print "Total de solicitações: $total_requests\n";
print "\n";

print "Solicitações por código de status:\n";
foreach my $status (sort keys %requests_by_status) {
    print "$status: $requests_by_status{$status}\n";
}
print "\n";

print "Solicitações por endereço IP:\n";
foreach my $ip (sort keys %requests_by_ip) {
    print "$ip: $requests_by_ip{$ip}\n";
}
```

Este código abre um arquivo de log chamado "access.log", lê cada linha do arquivo e extrai o endereço IP e o código de status de cada linha. Em seguida, ele contabiliza o número de solicitações por código de status e por endereço IP. Finalmente, ele imprime as estatísticas na saída.

Por favor, note que este é apenas um exemplo de código complexo em Perl. Existem muitas outras maneiras de escrever código complexo, dependendo dos requisitos específicos do projeto.