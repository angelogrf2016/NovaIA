Aqui está um exemplo de código complexo em Kotlin:

```
data class Person(val name: String, val age: Int)

fun main() {
    val peopleList = listOf(
        Person("John", 25),
        Person("Sarah", 30),
        Person("Michael", 40),
        Person("Emily", 35)
    )

    val filteredPeople = peopleList.filter { it.age > 30 }
    val sortedPeople = filteredPeople.sortedBy { it.name }
    val names = sortedPeople.map { it.name }

    println("Names of people aged over 30, sorted alphabetically: $names")
}
```

Neste código, criamos uma classe `Person` com propriedades `name` e `age`. Em seguida, definimos uma lista de pessoas e filtramos aquelas com idade superior a 30 anos. Em seguida, ordenamos essas pessoas pelo nome e, finalmente, mapeamos seus nomes em uma lista separada.

O resultado é impresso no console, mostrando os nomes das pessoas com idade superior a 30 anos, ordenados alfabeticamente.