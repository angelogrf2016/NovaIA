Aqui está um exemplo de código complexo em Dart, que utiliza várias classes, herança e interfaces:

```dart
abstract class Animal {
  void emitSound();
}

class Cat extends Animal {
  @override
  void emitSound() {
    print('Meow!');
  }
}

class Dog extends Animal {
  @override
  void emitSound() {
    print('Woof!');
  }
}

class Zoo {
  List<Animal> animals = [];

  void addAnimal(Animal animal) {
    animals.add(animal);
  }

  void makeAnimalsEmitSound() {
    for (Animal animal in animals) {
      animal.emitSound();
    }
  }
}

void main() {
  Cat cat = Cat();
  Dog dog = Dog();

  Zoo zoo = Zoo();
  zoo.addAnimal(cat);
  zoo.addAnimal(dog);

  zoo.makeAnimalsEmitSound();
}
```

Neste código, temos três classes: `Animal`, `Cat` e `Dog`. A classe `Animal` é abstrata e possui um método `emitSound()` que deve ser implementado pelas classes filhas. As classes `Cat` e `Dog` herdam da classe `Animal` e implementam o método `emitSound()`.

A classe `Zoo` possui uma lista de animais e métodos para adicionar animais à lista e fazer com que todos os animais emitam som. No método `main()`, criamos uma instância de `Cat` e `Dog`, em seguida, criamos uma instância de `Zoo` e adicionamos os animais à lista. Por fim, chamamos o método `makeAnimalsEmitSound()` para fazer com que todos os animais emitam som.