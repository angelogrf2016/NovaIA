Claro! Aqui está um exemplo de código complexo em Swift que envolve o uso de classes, herança, protocolos, extensões e manipulação de coleções:

```swift
// Definindo um protocolo para um animal
protocol Animal {
    var name: String { get }
    func makeSound()
}

// Definindo uma classe base para um mamífero
class Mammal: Animal {
    var name: String
    
    init(name: String) {
        self.name = name
    }
    
    func makeSound() {
        print("Mamífero fazendo barulho")
    }
}

// Definindo uma classe para um cachorro, que herda de Mamífero
class Dog: Mammal {
    override func makeSound() {
        print("Au au!")
    }
}

// Definindo uma extensão para o protocolo Animal que adiciona um método de alimentação
extension Animal {
    func feed() {
        print("\(name) sendo alimentado")
    }
}

// Criando uma lista de animais
var animals: [Animal] = []

// Adicionando um cachorro à lista de animais
let dog = Dog(name: "Rex")
animals.append(dog)

// Chamando o método makeSound e feed para cada animal na lista
for animal in animals {
    animal.makeSound()
    animal.feed()
}
```

Este código define um protocolo chamado "Animal" que exige que qualquer tipo que o adote tenha uma propriedade "name" e um método "makeSound". Em seguida, uma classe base "Mammal" é definida que implementa o protocolo Animal e adiciona uma propriedade "name" e uma implementação básica para o método "makeSound".

Uma classe "Dog" é então definida, que herda de "Mammal" e substitui a implementação do método "makeSound" para fazer o som de um cachorro.

Uma extensão para o protocolo Animal é definida para adicionar um método "feed", que imprime uma mensagem indicando que o animal está sendo alimentado. 

Em seguida, é criada uma lista de animais e um cachorro é adicionado a essa lista. Um loop é feito através da lista de animais, chamando o método "makeSound" e "feed" para cada animal.