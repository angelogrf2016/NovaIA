Claro! Aqui está um exemplo de código complexo em Go que envolve manipulação de arquivos, concorrência e uso de estruturas de dados:

```go
package main

import (
	"bufio"
	"fmt"
	"os"
	"sync"
)

func main() {
	// Abrir o arquivo de entrada
	file, err := os.Open("input.txt")
	if err != nil {
		fmt.Println("Erro ao abrir o arquivo:", err)
		return
	}
	defer file.Close()

	// Ler o conteúdo do arquivo linha por linha
	scanner := bufio.NewScanner(file)
	var lines []string
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}

	if err := scanner.Err(); err != nil {
		fmt.Println("Erro ao ler o arquivo:", err)
		return
	}

	// Criar um mapa para armazenar as palavras e suas contagens
	wordCounts := make(map[string]int)
	var wg sync.WaitGroup
	var mu sync.Mutex

	// Processar cada linha em paralelo
	for _, line := range lines {
		wg.Add(1)
		go func(line string) {
			defer wg.Done()

			// Dividir a linha em palavras
			words := splitWords(line)

			// Atualizar as contagens das palavras
			mu.Lock()
			for _, word := range words {
				wordCounts[word]++
			}
			mu.Unlock()
		}(line)
	}

	// Aguardar a conclusão de todas as goroutines
	wg.Wait()

	// Imprimir as contagens das palavras
	for word, count := range wordCounts {
		fmt.Printf("%s: %d\n", word, count)
	}
}

// Função auxiliar para dividir uma linha em palavras
func splitWords(line string) []string {
	var words []string
	currWord := ""
	for _, char := range line {
		if char == ' ' {
			if len(currWord) > 0 {
				words = append(words, currWord)
				currWord = ""
			}
		} else {
			currWord += string(char)
		}
	}
	if len(currWord) > 0 {
		words = append(words, currWord)
	}
	return words
}
```

Este código lê um arquivo chamado "input.txt" linha por linha, divide cada linha em palavras e conta quantas vezes cada palavra aparece no arquivo. Ele usa concorrência para processar cada linha em paralelo e um mapa com um mutex para armazenar as contagens das palavras. No final, exibe as contagens no console.