Aqui está um exemplo de código complexo em F# que envolve a manipulação de listas, recursão e funções de ordem superior:

```fsharp
// Função para calcular o fatorial de um número
let rec fatorial n =
    if n <= 1 then 1
    else n * fatorial (n - 1)

// Função para verificar se um número é primo
let isPrimo n =
    let rec isPrimoHelper divisor =
        if divisor >= n then true
        else if n % divisor = 0 then false
        else isPrimoHelper (divisor + 1)
    if n <= 1 then false
    else isPrimoHelper 2

// Função para calcular a soma dos fatoriais dos números primos em uma lista
let somaFatoriaisPrimos lista =
    lista
    |> List.filter isPrimo
    |> List.map fatorial
    |> List.sum

// Exemplo de uso da função somaFatoriaisPrimos
let listaExemplo = [2; 3; 4; 5; 6; 7; 8; 9; 10]
let resultado = somaFatoriaisPrimos listaExemplo
printfn "A soma dos fatoriais dos números primos na lista é: %d" resultado
```

Neste exemplo, temos algumas funções definidas:

- `fatorial` é uma função recursiva que calcula o fatorial de um número.
- `isPrimo` é uma função que verifica se um número é primo, utilizando uma função auxiliar `isPrimoHelper` recursiva.
- `somaFatoriaisPrimos` é uma função que recebe uma lista de números, filtra os números primos, calcula o fatorial de cada número primo e retorna a soma dos fatoriais.

No exemplo de uso, definimos uma lista de números de exemplo e chamamos a função `somaFatoriaisPrimos`, passando essa lista como argumento. O resultado é impresso no console.