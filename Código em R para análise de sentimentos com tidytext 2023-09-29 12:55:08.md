Claro! Aqui está um exemplo de um código complexo em R que realiza a análise de sentimentos em um conjunto de textos usando a biblioteca `tidytext`:

```R
# Carregar bibliotecas
library(tidyverse)
library(tidytext)
library(lexicon)
library(stringr)

# Carregar dados
dados <- read_csv("dados_texto.csv")

# Realizar pré-processamento dos textos
dados_preprocessados <- dados %>%
  mutate(texto_preprocessado = tolower(texto)) %>%
  unnest_tokens(palavra, texto_preprocessado)

# Remover stopwords
dados_sem_stopwords <- dados_preprocessados %>%
  anti_join(stop_words)

# Realizar análise de sentimentos
dados_sentimentos <- dados_sem_stopwords %>%
  inner_join(get_sentiments("bing")) %>%
  count(sentimento) %>%
  mutate(porcentagem = n / sum(n) * 100)

# Visualizar resultados
ggplot(dados_sentimentos, aes(x = sentimento, y = porcentagem)) +
  geom_bar(stat = "identity", fill = "steelblue") +
  labs(title = "Análise de Sentimentos", x = "Sentimento", y = "Porcentagem") +
  theme_minimal()
```

Neste exemplo, assumimos que os dados estão armazenados em um arquivo CSV chamado "dados_texto.csv" com uma coluna chamada "texto". O código realiza o pré-processamento dos textos, removendo stopwords (palavras comuns que não agregam significado ao sentimento), e em seguida, realiza a análise de sentimentos usando o dicionário de sentimentos "bing". Por fim, o código exibe um gráfico de barras com a distribuição dos sentimentos nos textos.