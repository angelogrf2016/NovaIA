Claro! Aqui está um exemplo de código complexo em Kotlin que envolve a criação de uma classe de árvore binária de pesquisa:

```kotlin
class TreeNode(var value: Int) {
    var left: TreeNode? = null
    var right: TreeNode? = null
}

class BinarySearchTree {
    private var root: TreeNode? = null
    
    fun insert(value: Int) {
        root = insertRecursive(root, value)
    }
    
    private fun insertRecursive(node: TreeNode?, value: Int): TreeNode {
        if (node == null) {
            return TreeNode(value)
        }
        
        if (value < node.value) {
            node.left = insertRecursive(node.left, value)
        } else if (value > node.value) {
            node.right = insertRecursive(node.right, value)
        }
        
        return node
    }
    
    fun search(value: Int): Boolean {
        return searchRecursive(root, value)
    }
    
    private fun searchRecursive(node: TreeNode?, value: Int): Boolean {
        if (node == null) {
            return false
        }
        
        if (value == node.value) {
            return true
        }
        
        return if (value < node.value) {
            searchRecursive(node.left, value)
        } else {
            searchRecursive(node.right, value)
        }
    }
    
    fun printInOrder() {
        printInOrderRecursive(root)
        println()
    }
    
    private fun printInOrderRecursive(node: TreeNode?) {
        if (node != null) {
            printInOrderRecursive(node.left)
            print("${node.value} ")
            printInOrderRecursive(node.right)
        }
    }
}

fun main() {
    val bst = BinarySearchTree()
    
    bst.insert(5)
    bst.insert(3)
    bst.insert(7)
    bst.insert(2)
    bst.insert(4)
    bst.insert(6)
    bst.insert(8)
    
    bst.printInOrder() // Output: 2 3 4 5 6 7 8
    
    println(bst.search(5)) // Output: true
    println(bst.search(10)) // Output: false
}
```

Nesse exemplo, criamos uma classe `TreeNode` que representa um nó em uma árvore binária de pesquisa. Em seguida, implementamos a classe `BinarySearchTree`, que possui métodos para inserir valores na árvore, pesquisar por valores na árvore e imprimir a árvore em ordem.

No método `main`, criamos uma instância da árvore binária de pesquisa, inserimos alguns valores e, em seguida, imprimimos a árvore em ordem. Também fazemos algumas pesquisas na árvore para verificar se um determinado valor existe ou não.