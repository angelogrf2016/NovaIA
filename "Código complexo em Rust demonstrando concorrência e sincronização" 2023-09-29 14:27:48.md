Claro! Aqui está um exemplo de código complexo em Rust que utiliza algumas funcionalidades avançadas da linguagem:

```rust
// Importando bibliotecas
use std::thread;
use std::sync::{Arc, Mutex};
use std::time::Duration;

fn main() {
    // Criando um vetor compartilhado entre threads
    let shared_vector = Arc::new(Mutex::new(vec![1, 2, 3, 4, 5]));

    // Criando duas threads que manipulam o vetor compartilhado
    let thread1 = thread::spawn({
        let shared_vector = Arc::clone(&shared_vector);
        move || {
            let mut vector = shared_vector.lock().unwrap();
            vector.push(6);
            vector.push(7);
            vector.push(8);
            println!("Thread 1: {:?}", vector);
            thread::sleep(Duration::from_secs(1));
        }
    });

    let thread2 = thread::spawn({
        let shared_vector = Arc::clone(&shared_vector);
        move || {
            let mut vector = shared_vector.lock().unwrap();
            vector.push(9);
            vector.push(10);
            println!("Thread 2: {:?}", vector);
            thread::sleep(Duration::from_secs(1));
        }
    });

    // Aguardando as threads terminarem
    thread1.join().unwrap();
    thread2.join().unwrap();

    // Imprimindo o estado final do vetor compartilhado
    let vector = shared_vector.lock().unwrap();
    println!("Final vector: {:?}", vector);
}
```

Neste exemplo, estamos utilizando a biblioteca padrão do Rust para criar duas threads que manipulam um vetor compartilhado. Utilizamos um `Arc<Mutex<Vec<i32>>>` para garantir acesso seguro ao vetor entre as threads.

Cada thread adiciona elementos ao vetor e, em seguida, imprime seu estado atual. O programa principal aguarda as threads terminarem e, em seguida, imprime o estado final do vetor compartilhado.

Este código demonstra o uso de concorrência em Rust, bem como a utilização de tipos seguros e ferramentas de sincronização para evitar race conditions e problemas de acesso concorrente.