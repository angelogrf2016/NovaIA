Claro! Aqui está um código em Julia que calcula a soma dos fatoriais de um dado número e verifica se o resultado é um número primo:

```julia
function fatorial(n)
    if n == 0
        return 1
    else
        return n * fatorial(n-1)
    end
end

function soma_fatorial_primo(numero)
    soma = 0
    for i in 1:numero
        soma += fatorial(i)
    end
    
    if isprime(soma)
        println("A soma dos fatoriais de $numero é um número primo.")
    else
        println("A soma dos fatoriais de $numero não é um número primo.")
    end
end

numero = 5
soma_fatorial_primo(numero)
```

Este código define duas funções: `fatorial` e `soma_fatorial_primo`. A primeira função calcula o fatorial de um número usando uma recursão simples. A segunda função calcula a soma dos fatoriais de todos os números de 1 até o número fornecido como entrada. Em seguida, verifica se essa soma é um número primo usando a função `isprime`. No exemplo acima, o número fornecido é 5, mas você pode alterá-lo para testar com outros valores.