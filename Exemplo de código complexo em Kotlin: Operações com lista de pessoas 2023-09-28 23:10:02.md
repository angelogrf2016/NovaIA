Aqui está um exemplo de código complexo em Kotlin:

```kotlin
import java.util.*

data class Person(val name: String, val age: Int)

fun main() {
    val people = listOf(
        Person("Alice", 25),
        Person("Bob", 30),
        Person("Charlie", 35),
        Person("Dave", 40),
        Person("Eve", 45),
        Person("Frank", 50)
    )

    val oldestPerson = people.maxByOrNull { it.age }
    println("A pessoa mais velha é ${oldestPerson?.name}")

    val under40 = people.filter { it.age < 40 }
    println("Pessoas com menos de 40 anos: $under40")

    val sortedByName = people.sortedBy { it.name }
    println("Pessoas ordenadas por nome: $sortedByName")

    val groupedByAge = people.groupBy { it.age }
    println("Pessoas agrupadas por idade: $groupedByAge")

    val random = Random()
    val randomNumber = random.nextInt(100)
    println("Número aleatório: $randomNumber")
}
```

Neste exemplo, temos uma classe `Person` que representa uma pessoa com nome e idade. No método `main`, criamos uma lista de pessoas e realizamos algumas operações com ela.

Primeiro, usamos a função `maxByOrNull` para encontrar a pessoa mais velha da lista. Em seguida, usamos a função `filter` para filtrar as pessoas com menos de 40 anos. Depois, usamos a função `sortedBy` para ordenar as pessoas por nome e a função `groupBy` para agrupá-las por idade.

Por fim, usamos a classe `Random` para gerar um número aleatório entre 0 e 100. Todo o código é escrito em Kotlin, uma linguagem moderna e concisa que roda na JVM.