defmodule ComplexCode do
  def calculate_sum_of_primes(limit) do
    primes = find_primes(limit)
    sum = Enum.reduce(primes, 0, fn prime, acc -> prime + acc end)
    IO.puts("The sum of primes up to #{limit} is #{sum}.")
  end

  def find_primes(limit) do
    Enum.filter(2..limit, fn n -> is_prime(n) end)
  end

  defp is_prime(n) do
    case n do
      2 -> true
      _ when rem(n, 2) == 0 -> false
      _ -> is_prime_helper(n, 3)
    end
  end

  defp is_prime_helper(n, divisor) do
    if divisor * divisor > n do
      true
    else
      rem(n, divisor) != 0 && is_prime_helper(n, divisor + 2)
    end
  end
end

ComplexCode.calculate_sum_of_primes(1000)
ComplexCode.calculate_sum_of_primes(10000)
ComplexCode.calculate_sum_of_primes(100000)
ComplexCode.calculate_sum_of_primes(1000000)
ComplexCode.calculate_sum_of_primes(10000000)
ComplexCode.calculate_sum_of_primes(100000000)
ComplexCode.calculate_sum_of_primes(1000000000)

A função calculate_sum_of_primes recebe um número limite como argumento e retorna a soma de todos os números primos até esse limite. O código utiliza a função find_primes para encontrar todos os números primos no intervalo de 2 até o limite. Em seguida, utiliza a função Enum.reduce para somar todos os números primos encontrados. Por fim, imprime a soma na tela.

A função is_prime é utilizada para verificar se um número é primo. Ela utiliza uma estrutura de caso para tratar os casos especiais de 2 e de números pares. Caso contrário, chama a função is_prime_helper para realizar a verificação de números ímpares.

A função is_prime_helper é uma função auxiliar que utiliza recursão para verificar se um número é primo. Ela recebe o número a ser verificado e um divisor inicial, que começa em 3. A recursão é feita até que o quadrado do divisor seja maior que o número a ser verificado, momento em que retorna true. Caso contrário, verifica se o número não é divisível pelo divisor atual e chama a função novamente incrementando o divisor em 2.

No final do código, são feitas chamadas à função calculate_sum_of_primes com diferentes valores de limite para testar o código.