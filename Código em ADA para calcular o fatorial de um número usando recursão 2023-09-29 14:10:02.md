Aqui está um exemplo de código em ADA que calcula o fatorial de um número usando um procedimento recursivo:

```ada
with Ada.Text_IO;

procedure Fatorial is

   function Calcula_Fatorial(N: Natural) return Natural is
   begin
      if N = 0 then
         return 1;
      else
         return N * Calcula_Fatorial(N - 1);
      end if;
   end Calcula_Fatorial;

   Numero: Natural;
begin
   Ada.Text_IO.Put("Digite um número inteiro positivo: ");
   Ada.Text_IO.Get(Numero);

   Ada.Text_IO.New_Line;
   Ada.Text_IO.Put("O fatorial de " & Numero'Image & " é: ");
   Ada.Text_IO.Put(Calcula_Fatorial(Numero));
   Ada.Text_IO.New_Line;
end Fatorial;
```

Este código utiliza um procedimento chamado `Calcula_Fatorial` que recebe um número inteiro positivo `N` e retorna o fatorial desse número. Ele é implementado de forma recursiva, onde o caso base é quando `N` é igual a zero e retorna 1. Caso contrário, ele calcula o fatorial de `N - 1` e multiplica pelo número `N`.

No procedimento principal `Fatorial`, é solicitado ao usuário que digite um número inteiro positivo. Em seguida, é chamado o procedimento `Calcula_Fatorial` passando esse número e o resultado é exibido na tela.