Claro! Aqui está um exemplo de código complexo em Python que simula uma rede neural para reconhecimento de dígitos escritos à mão usando o conjunto de dados MNIST:

```python
import numpy as np
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score

# Função de ativação sigmoid
def sigmoid(x):
    return 1 / (1 + np.exp(-x))

# Derivada da função de ativação sigmoid
def sigmoid_derivative(x):
    return x * (1 - x)

# Classe da rede neural
class NeuralNetwork:
    def __init__(self, input_size, hidden_size, output_size):
        self.input_size = input_size
        self.hidden_size = hidden_size
        self.output_size = output_size
        
        self.W1 = np.random.randn(self.input_size, self.hidden_size)
        self.b1 = np.zeros((1, self.hidden_size))
        
        self.W2 = np.random.randn(self.hidden_size, self.output_size)
        self.b2 = np.zeros((1, self.output_size))
        
    def forward(self, X):
        self.z1 = np.dot(X, self.W1) + self.b1
        self.a1 = sigmoid(self.z1)
        
        self.z2 = np.dot(self.a1, self.W2) + self.b2
        self.a2 = sigmoid(self.z2)
        
        return self.a2
    
    def backward(self, X, y, output):
        self.error = output - y
        self.delta = self.error * sigmoid_derivative(output)
        
        self.W2_gradient = np.dot(self.a1.T, self.delta)
        self.b2_gradient = np.sum(self.delta, axis=0, keepdims=True)
        
        self.delta_hidden = np.dot(self.delta, self.W2.T) * sigmoid_derivative(self.a1)
        
        self.W1_gradient = np.dot(X.T, self.delta_hidden)
        self.b1_gradient = np.sum(self.delta_hidden, axis=0)
        
    def update_weights(self, learning_rate):
        self.W1 -= learning_rate * self.W1_gradient
        self.b1 -= learning_rate * self.b1_gradient
        
        self.W2 -= learning_rate * self.W2_gradient
        self.b2 -= learning_rate * self.b2_gradient
        
    def train(self, X, y, epochs, learning_rate):
        self.loss = []
        
        for epoch in range(epochs):
            output = self.forward(X)
            self.backward(X, y, output)
            self.update_weights(learning_rate)
            
            loss = np.mean(np.abs(self.error))
            self.loss.append(loss)
            
            if epoch % 100 == 0:
                print(f"Epoch {epoch}/{epochs} - Loss: {loss}")
                
    def predict(self, X):
        return np.round(self.forward(X))
    
# Carregar o conjunto de dados MNIST
from sklearn.datasets import fetch_openml

mnist = fetch_openml('mnist_784', version=1)
X = mnist['data']
y = mnist['target']

# Pré-processar os dados
X /= 255
y = y.astype(int)

# Dividir os dados em treinamento e teste
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

# Criar e treinar a rede neural
input_size = X_train.shape[1]
hidden_size = 64
output_size = 10

nn = NeuralNetwork(input_size, hidden_size, output_size)
nn.train(X_train, y_train, epochs=1000, learning_rate=0.1)

# Avaliar a acurácia da rede neural nos dados de teste
y_pred = nn.predict(X_test)
accuracy = accuracy_score(y_test, y_pred)

print(f"Acurácia: {accuracy}")
```

Este código implementa uma rede neural com uma camada oculta para reconhecer dígitos escritos à mão. O conjunto de dados MNIST é carregado e pré-processado, dividido em dados de treinamento e teste. A rede neural é criada com uma camada de entrada do tamanho dos recursos, uma camada oculta com 64 neurônios e uma camada de saída com 10 neurônios correspondentes aos dígitos de 0 a 9. A função de ativação sigmoid é usada nas camadas oculta e de saída. A rede neural é treinada usando a função de perda média absoluta e o algoritmo de retropropagação. A acurácia da rede neural é calculada nos dados de teste e exibida.