Aqui está um exemplo de código complexo em Perl que realiza uma análise de texto para contar a frequência de palavras em um determinado arquivo:

```perl
#!/usr/bin/perl

use strict;
use warnings;

# Abre o arquivo para leitura
open(my $file, "<", "texto.txt") or die "Não foi possível abrir o arquivo: $!";

# Cria um hash vazio para armazenar a frequência das palavras
my %frequencia;

# Loop pelo arquivo linha por linha
while (my $linha = <$file>) {
    # Remove os caracteres especiais e converte para letras minúsculas
    $linha =~ s/[^\w\s]//g;
    $linha = lc($linha);
    
    # Divide a linha em palavras
    my @palavras = split(/\s+/, $linha);
    
    # Incrementa a frequência de cada palavra no hash
    foreach my $palavra (@palavras) {
        $frequencia{$palavra}++;
    }
}

# Fecha o arquivo
close($file);

# Imprime a frequência de cada palavra
foreach my $palavra (sort keys %frequencia) {
    print "$palavra: $frequencia{$palavra}\n";
}
```

Neste exemplo, o código abre o arquivo "texto.txt" para leitura e, em seguida, itera por cada linha do arquivo. Em cada linha, os caracteres especiais são removidos e todas as letras são convertidas para minúsculas.

A linha é dividida em palavras usando o caractere de espaço como separador e, em seguida, a frequência de cada palavra é incrementada em um hash `%frequencia`.

No final, o código imprime a frequência de cada palavra em ordem alfabética.