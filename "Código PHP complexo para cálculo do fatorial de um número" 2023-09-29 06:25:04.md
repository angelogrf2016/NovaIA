Aqui está um exemplo de código PHP complexo, que calcula o fatorial de um número:

```php
<?php

function fatorial($n) {
    if ($n == 0 || $n == 1) {
        return 1;
    } else {
        return $n * fatorial($n - 1);
    }
}

$num = 5;
$resultado = fatorial($num);

echo "O fatorial de $num é $resultado.";

?>
```

Neste exemplo, temos uma função chamada `fatorial` que recebe um número `$n` como parâmetro. A função verifica se o número é igual a 0 ou 1 e retorna 1 nesses casos. Caso contrário, a função chama a si mesma recursivamente, passando o número decrementado em 1, e multiplica o resultado pelo número atual.

Em seguida, definimos uma variável `$num` com o valor 5 e chamamos a função `fatorial` passando esse valor como argumento. O resultado é armazenado na variável `$resultado`.

Por fim, exibimos uma mensagem na tela com o valor do número e o seu fatorial. Neste caso, o resultado será "O fatorial de 5 é 120".