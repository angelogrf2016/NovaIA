Claro! Aqui está um código complexo em Python que gera uma sequência de números primos usando o Crivo de Eratóstenes:

```python
def encontrar_primos(n):
    # Criar uma lista de números de 2 a n
    numeros = [True] * (n+1)
    numeros[0] = numeros[1] = False

    # Aplicar o Crivo de Eratóstenes
    for i in range(2, int(n**0.5) + 1):
        if numeros[i]:
            for j in range(i*i, n+1, i):
                numeros[j] = False

    # Retornar os números primos
    primos = [i for i in range(n+1) if numeros[i]]
    return primos

# Exemplo de uso
limite_superior = 100
primos = encontrar_primos(limite_superior)
print("Números primos até", limite_superior, ":", primos)
```

Este código utiliza o Crivo de Eratóstenes para encontrar todos os números primos até um determinado limite superior (no exemplo, 100). Ele cria uma lista de booleanos chamada `numeros`, onde `numeros[i]` é `True` se `i` é primo e `False` caso contrário.

Em seguida, o Crivo de Eratóstenes é aplicado, percorrendo todos os números de 2 até a raiz quadrada de `n` (o limite superior). Se `numeros[i]` for `True`, significa que `i` é um número primo, então todos os múltiplos de `i` são marcados como `False` na lista `numeros`.

Finalmente, o código cria uma lista chamada `primos` que contém todos os números primos encontrados, e a imprime na tela. No exemplo, os números primos até 100 são: [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97].