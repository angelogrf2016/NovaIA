Aqui está um código complexo em PHP que gera uma sequência de números primos:

```php
<?php

// Função para verificar se um número é primo
function isPrime($number) {
    if ($number <= 1) {
        return false;
    }
    
    for ($i = 2; $i <= sqrt($number); $i++) {
        if ($number % $i == 0) {
            return false;
        }
    }
    
    return true;
}

// Função para gerar uma sequência de números primos
function generatePrimeSequence($length) {
    $sequence = [];
    $number = 2;
    
    while (count($sequence) < $length) {
        if (isPrime($number)) {
            $sequence[] = $number;
        }
        
        $number++;
    }
    
    return $sequence;
}

// Gerar uma sequência de 10 números primos
$primeSequence = generatePrimeSequence(10);

// Imprimir a sequência de números primos gerada
echo "Sequência de números primos gerada: ";
foreach ($primeSequence as $prime) {
    echo $prime . " ";
}

?>
```

Este código utiliza duas funções: `isPrime` para verificar se um número é primo e `generatePrimeSequence` para gerar uma sequência de números primos com base em um comprimento fornecido. A função `isPrime` utiliza um algoritmo simples para verificar se um número é divisível por qualquer número entre 2 e a raiz quadrada do número em questão. A função `generatePrimeSequence` utiliza a função `isPrime` para iterar e adicionar números primos à sequência até que o comprimento desejado seja atingido.

No exemplo acima, é gerada uma sequência de 10 números primos e, em seguida, a sequência é impressa na tela.