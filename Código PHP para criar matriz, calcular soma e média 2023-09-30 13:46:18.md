Claro! Aqui está um código complexo em PHP que cria uma matriz bidimensional com números aleatórios e depois realiza algumas operações com os valores:

```php
<?php

// Função para preencher a matriz com números aleatórios
function preencherMatriz($linhas, $colunas)
{
    $matriz = array();
    for ($i = 0; $i < $linhas; $i++) {
        $linha = array();
        for ($j = 0; $j < $colunas; $j++) {
            $linha[] = rand(1, 100); // Números aleatórios entre 1 e 100
        }
        $matriz[] = $linha;
    }
    return $matriz;
}

// Função para imprimir a matriz
function imprimirMatriz($matriz)
{
    foreach ($matriz as $linha) {
        foreach ($linha as $valor) {
            echo $valor . " ";
        }
        echo "\n";
    }
}

// Função para calcular a soma dos valores da matriz
function calcularSoma($matriz)
{
    $soma = 0;
    foreach ($matriz as $linha) {
        foreach ($linha as $valor) {
            $soma += $valor;
        }
    }
    return $soma;
}

// Função para calcular a média dos valores da matriz
function calcularMedia($matriz)
{
    $totalValores = 0;
    $totalElementos = 0;
    foreach ($matriz as $linha) {
        foreach ($linha as $valor) {
            $totalValores += $valor;
            $totalElementos++;
        }
    }
    return $totalValores / $totalElementos;
}

// Criar matriz 5x5 com números aleatórios
$matriz = preencherMatriz(5, 5);

// Imprimir matriz
echo "Matriz gerada:\n";
imprimirMatriz($matriz);

// Calcular e imprimir a soma dos valores da matriz
$soma = calcularSoma($matriz);
echo "\nSoma dos valores: " . $soma;

// Calcular e imprimir a média dos valores da matriz
$media = calcularMedia($matriz);
echo "\nMédia dos valores: " . $media;

?>
```

Este código cria uma matriz 5x5 preenchida com números aleatórios entre 1 e 100. Em seguida, ele imprime a matriz gerada, calcula a soma de todos os valores da matriz e calcula a média dos valores.