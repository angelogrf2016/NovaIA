Claro! Aqui está um exemplo de código em Objective-C que envolve conceitos mais complexos:

```objective-c
#import <Foundation/Foundation.h>

// Classe Animal
@interface Animal : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic) NSInteger age;

- (instancetype)initWithName:(NSString *)name age:(NSInteger)age;
- (void)makeSound;

@end

@implementation Animal

- (instancetype)initWithName:(NSString *)name age:(NSInteger)age {
    self = [super init];
    if (self) {
        _name = name;
        _age = age;
    }
    return self;
}

- (void)makeSound {
    NSLog(@"%@ is making a sound.", self.name);
}

@end

// Classe Dog, que herda de Animal
@interface Dog : Animal

@property (nonatomic, strong) NSString *breed;

- (instancetype)initWithName:(NSString *)name age:(NSInteger)age breed:(NSString *)breed;
- (void)bark;

@end

@implementation Dog

- (instancetype)initWithName:(NSString *)name age:(NSInteger)age breed:(NSString *)breed {
    self = [super initWithName:name age:age];
    if (self) {
        _breed = breed;
    }
    return self;
}

- (void)bark {
    NSLog(@"%@ is barking.", self.name);
}

@end

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        // Criando um objeto Dog
        Dog *myDog = [[Dog alloc] initWithName:@"Max" age:5 breed:@"Labrador"];
        
        // Chamando métodos do objeto Dog
        [myDog makeSound];
        [myDog bark];
        
        // Acessando propriedades do objeto Dog
        NSLog(@"Name: %@", myDog.name);
        NSLog(@"Age: %ld", (long)myDog.age);
        NSLog(@"Breed: %@", myDog.breed);
    }
    return 0;
}
```

Neste código, temos duas classes: `Animal` e `Dog`. A classe `Dog` herda da classe `Animal`. Ambas as classes possuem propriedades e métodos. O método `main` cria um objeto `Dog`, chama métodos e acessa propriedades desse objeto.