Claro! Aqui está um código complexo em Kotlin:

```kotlin
fun main() {
    val numeros = mutableListOf<Int>()
    for (i in 1..100) {
        if (i % 3 == 0 && i % 5 == 0) {
            numeros.add(i)
        }
    }
    numeros.sortDescending()
    println("Números divisíveis por 3 e 5 em ordem decrescente: $numeros")
    val soma = numeros.sum()
    println("Soma dos números: $soma")
    val media = soma.toDouble() / numeros.size
    println("Média dos números: $media")
    val multiplicacao = numeros.reduce { acc, i -> acc * i }
    println("Multiplicação dos números: $multiplicacao")
}
```

Neste código, criamos uma lista chamada "numeros" para armazenar todos os números entre 1 e 100 que são divisíveis por 3 e 5. Em seguida, ordenamos a lista em ordem decrescente e a imprimimos.

Calculamos a soma dos números, a média e a multiplicação de todos os elementos da lista.

Espero que esse código atenda às suas expectativas!