Claro! Aqui está um código complexo em Go que envolve concorrência, canais e goroutines para resolver o problema clássico dos produtores e consumidores:

```go
package main

import (
	"fmt"
	"math/rand"
	"sync"
	"time"
)

const (
	NumWorkers   = 5
	BufferSize   = 10
	NumProducers = 3
	NumItems     = 20
)

func main() {
	wg := sync.WaitGroup{}
	buffer := make(chan int, BufferSize)
	done := make(chan bool)

	// Inicia os produtores
	for i := 0; i < NumProducers; i++ {
		wg.Add(1)
		go producer(i, buffer, &wg)
	}

	// Inicia os consumidores
	for i := 0; i < NumWorkers; i++ {
		wg.Add(1)
		go worker(i, buffer, &wg)
	}

	// Aguarda todos os produtores terminarem
	wg.Wait()

	// Sinaliza que não há mais itens para consumir
	done <- true

	// Aguarda todos os consumidores terminarem
	wg.Wait()
}

func producer(id int, buffer chan<- int, wg *sync.WaitGroup) {
	defer wg.Done()

	for i := 0; i < NumItems; i++ {
		// Gera um item aleatório
		item := rand.Intn(100)

		// Envia o item para o buffer
		buffer <- item

		fmt.Printf("Produtor %d: item %d enviado\n", id, item)

		// Aguarda um tempo aleatório antes de produzir o próximo item
		time.Sleep(time.Duration(rand.Intn(1000)) * time.Millisecond)
	}
}

func worker(id int, buffer <-chan int, wg *sync.WaitGroup) {
	defer wg.Done()

	for {
		// Recebe um item do buffer
		item, ok := <-buffer
		if !ok {
			// O buffer está vazio e fechado, não há mais itens para consumir
			fmt.Printf("Consumidor %d: não há mais itens para consumir\n", id)
			return
		}

		fmt.Printf("Consumidor %d: item %d consumido\n", id, item)

		// Aguarda um tempo aleatório antes de consumir o próximo item
		time.Sleep(time.Duration(rand.Intn(1000)) * time.Millisecond)
	}
}
```

Neste exemplo, há um número fixo de produtores e consumidores que compartilham um buffer de tamanho fixo. Os produtores geram itens aleatórios e os enviam para o buffer, enquanto os consumidores recebem os itens do buffer e os processam. 

A concorrência é implementada usando goroutines e canais. Os produtores e consumidores são executados como goroutines separadas e se comunicam usando o canal `buffer`. Os produtores enviam itens para o canal e os consumidores recebem os itens do canal. O tamanho do buffer controla a capacidade do buffer e permite que os produtores continuem produzindo itens mesmo que os consumidores estejam ocupados.

O programa principal cria as goroutines para os produtores e consumidores, aguarda todos os produtores terminarem e, em seguida, sinaliza que não há mais itens para consumir. Em seguida, aguarda todos os consumidores terminarem antes de encerrar o programa.

Espero que isso ajude!