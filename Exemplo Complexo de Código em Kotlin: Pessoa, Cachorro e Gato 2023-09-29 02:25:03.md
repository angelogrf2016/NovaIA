Aqui está um exemplo complexo de código em Kotlin:

```kotlin
class Person(val name: String, var age: Int) {
    fun greet() {
        println("Hello, my name is $name and I am $age years old.")
    }
}

interface Animal {
    fun makeSound()
}

class Dog(val name: String) : Animal {
    override fun makeSound() {
        println("$name says woof!")
    }
}

class Cat(val name: String) : Animal {
    override fun makeSound() {
        println("$name says meow!")
    }
}

fun main() {
    val person = Person("John", 25)
    person.greet()

    val dog = Dog("Buddy")
    val cat = Cat("Whiskers")

    val animals = listOf<Animal>(dog, cat)
    for (animal in animals) {
        animal.makeSound()
    }
}
```

Neste exemplo, temos uma classe `Person` que representa uma pessoa com um nome e idade. A classe possui um método `greet()` que imprime uma saudação com as informações da pessoa.

Também temos uma interface `Animal` com um método `makeSound()`. As classes `Dog` e `Cat` implementam essa interface e sobrescrevem o método `makeSound()` para imprimir o som característico de cada animal.

No `main()`, criamos uma instância de `Person` chamada `person` e chamamos o método `greet()` para imprimir a saudação.

Em seguida, criamos instâncias de `Dog` e `Cat` chamadas `dog` e `cat`, respectivamente.

Por fim, criamos uma lista de animais contendo `dog` e `cat` e percorremos essa lista chamando o método `makeSound()` para cada animal, imprimindo o som que cada um deles faz.