Aqui está um exemplo de código complexo em R:

```R
# Código para calcular a média, mediana e desvio padrão de um conjunto de números

# Definindo uma lista de números
numeros <- c(12, 24, 36, 48, 60)

# Função para calcular a média
calcular_media <- function(numeros) {
  media <- sum(numeros) / length(numeros)
  return(media)
}

# Função para calcular a mediana
calcular_mediana <- function(numeros) {
  mediana <- median(numeros)
  return(mediana)
}

# Função para calcular o desvio padrão
calcular_desvio_padrao <- function(numeros) {
  desvio_padrao <- sd(numeros)
  return(desvio_padrao)
}

# Chamando as funções e armazenando os resultados em variáveis
media <- calcular_media(numeros)
mediana <- calcular_mediana(numeros)
desvio_padrao <- calcular_desvio_padrao(numeros)

# Imprimindo os resultados
cat("A média dos números é:", media, "\n")
cat("A mediana dos números é:", mediana, "\n")
cat("O desvio padrão dos números é:", desvio_padrao, "\n")
```

Este código define uma lista de números e, em seguida, utiliza funções para calcular a média, mediana e desvio padrão desses números. Os resultados são armazenados em variáveis e, em seguida, impressos na tela.