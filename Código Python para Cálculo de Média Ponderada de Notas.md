Claro! Aqui está um exemplo de um código complexo em Python que envolve uma combinação de funções, loops e estruturas de dados:

```python
# Cálculo de média ponderada de notas de alunos

# Função para calcular a média ponderada
def calcular_media_ponderada(notas, pesos):
    soma = 0
    peso_total = sum(pesos)
    for nota, peso in zip(notas, pesos):
        soma += nota * peso
    return soma / peso_total

# Lista de notas dos alunos
notas_alunos = [[8, 7, 9], [6, 9, 5], [7, 8, 6], [9, 7, 8]]

# Lista de pesos das notas
pesos = [0.3, 0.3, 0.4]

# Lista para armazenar as médias dos alunos
medias_alunos = []

# Calcula a média ponderada para cada aluno e armazena na lista
for notas in notas_alunos:
    media = calcular_media_ponderada(notas, pesos)
    medias_alunos.append(media)

# Imprime as médias dos alunos
for i, media in enumerate(medias_alunos):
    print(f"A média do aluno {i+1} é {media:.2f}")
```

Neste exemplo, temos uma função `calcular_media_ponderada` que recebe uma lista de notas e uma lista de pesos e retorna a média ponderada dessas notas. Em seguida, temos uma lista de notas dos alunos e uma lista de pesos das notas.

Depois, utilizamos um loop `for` para percorrer cada lista de notas dos alunos, calcular a média ponderada utilizando a função `calcular_media_ponderada` e armazenar a média na lista `medias_alunos`.

Por fim, utilizamos outro loop `for` para imprimir as médias dos alunos.