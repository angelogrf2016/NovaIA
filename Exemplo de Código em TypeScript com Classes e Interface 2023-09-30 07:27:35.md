Claro! Aqui está um exemplo de um código complexo em TypeScript:

```typescript
interface Animal {
  name: string;
  age: number;
}

class Dog implements Animal {
  name: string;
  age: number;

  constructor(name: string, age: number) {
    this.name = name;
    this.age = age;
  }

  bark(): void {
    console.log("Woof woof!");
  }
}

class Cat implements Animal {
  name: string;
  age: number;

  constructor(name: string, age: number) {
    this.name = name;
    this.age = age;
  }

  meow(): void {
    console.log("Meow meow!");
  }
}

class AnimalShelter {
  animals: Animal[];

  constructor() {
    this.animals = [];
  }

  addAnimal(animal: Animal): void {
    this.animals.push(animal);
  }

  listAnimals(): void {
    this.animals.forEach((animal) => console.log(animal.name));
  }
}

const shelter = new AnimalShelter();
const dog = new Dog("Rex", 5);
const cat = new Cat("Whiskers", 3);

shelter.addAnimal(dog);
shelter.addAnimal(cat);

shelter.listAnimals();
```

Neste exemplo, temos uma interface `Animal` que define as propriedades `name` e `age`. Em seguida, temos as classes `Dog` e `Cat` que implementam essa interface. Cada uma dessas classes possui um construtor e um método específico (`bark` para cães e `meow` para gatos).

Também temos a classe `AnimalShelter`, que possui um array `animals` e métodos para adicionar animais (`addAnimal`) e listar os animais presentes no abrigo (`listAnimals`).

No final do código, criamos uma instância do abrigo (`shelter`), uma instância de um cão (`dog`) e uma instância de um gato (`cat`). Adicionamos esses animais ao abrigo e, em seguida, chamamos o método `listAnimals` para listar os animais presentes no abrigo (no caso, "Rex" e "Whiskers" serão exibidos no console).