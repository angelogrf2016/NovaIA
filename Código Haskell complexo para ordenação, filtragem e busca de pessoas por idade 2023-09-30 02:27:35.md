Aqui está um exemplo de código Haskell mais complexo que envolve várias funções e estruturas de dados:

```haskell
import Data.List (sortBy)
import Data.Ord (comparing)

data Pessoa = Pessoa { nome :: String, idade :: Int }

pessoas :: [Pessoa]
pessoas = [
    Pessoa "João" 25,
    Pessoa "Maria" 30,
    Pessoa "Pedro" 20,
    Pessoa "Ana" 35
    ]

ordenarPorIdade :: [Pessoa] -> [Pessoa]
ordenarPorIdade = sortBy (comparing idade)

filtrarPorIdade :: Int -> [Pessoa] -> [Pessoa]
filtrarPorIdade n = filter (\p -> idade p >= n)

maisVelho :: [Pessoa] -> Maybe Pessoa
maisVelho [] = Nothing
maisVelho xs = Just $ maximumBy (comparing idade) xs

main :: IO ()
main = do
    putStrLn "Pessoas ordenadas por idade:"
    mapM_ (putStrLn . nome) $ ordenarPorIdade pessoas
    
    putStrLn "\nPessoas com idade maior ou igual a 30:"
    mapM_ (putStrLn . nome) $ filtrarPorIdade 30 pessoas
    
    putStrLn "\nA pessoa mais velha:"
    case maisVelho pessoas of
        Nothing -> putStrLn "Não há pessoas."
        Just p -> putStrLn $ "Nome: " ++ nome p ++ ", Idade: " ++ show (idade p)
```

Neste código, definimos um tipo de dado `Pessoa` com os campos `nome` e `idade`. Em seguida, temos uma lista de pessoas chamada `pessoas`.

Em seguida, temos três funções: `ordenarPorIdade`, `filtrarPorIdade` e `maisVelho`. A função `ordenarPorIdade` ordena a lista de pessoas por idade. A função `filtrarPorIdade` filtra a lista de pessoas, retornando apenas aquelas com idade maior ou igual a um valor fornecido. A função `maisVelho` retorna a pessoa mais velha da lista.

No bloco `main`, imprimimos as pessoas ordenadas por idade, as pessoas com idade maior ou igual a 30 e a pessoa mais velha.