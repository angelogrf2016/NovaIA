Claro! Aqui está um código complexo em Ruby que utiliza conceitos avançados de programação:

```ruby
class Grafo
  attr_accessor :vertices, :arestas

  def initialize
    @vertices = []
    @arestas = {}
  end

  def adicionar_vertice(vertice)
    @vertices << vertice
    @arestas[vertice] = []
  end

  def adicionar_aresta(vertice_origem, vertice_destino)
    @arestas[vertice_origem] << vertice_destino
    @arestas[vertice_destino] << vertice_origem
  end

  def busca_em_largura(vertice_inicial)
    visitados = [vertice_inicial]
    fila = [vertice_inicial]

    while fila.any?
      vertice_atual = fila.shift

      @arestas[vertice_atual].each do |vertice_vizinho|
        next if visitados.include?(vertice_vizinho)

        visitados << vertice_vizinho
        fila << vertice_vizinho
      end
    end

    visitados
  end
end

grafo = Grafo.new
grafo.adicionar_vertice("A")
grafo.adicionar_vertice("B")
grafo.adicionar_vertice("C")
grafo.adicionar_vertice("D")
grafo.adicionar_vertice("E")
grafo.adicionar_aresta("A", "B")
grafo.adicionar_aresta("A", "C")
grafo.adicionar_aresta("B", "D")
grafo.adicionar_aresta("C", "E")

vertice_inicial = "A"
caminho = grafo.busca_em_largura(vertice_inicial)

puts "Caminho percorrido a partir do vértice #{vertice_inicial}:"
caminho.each { |vertice| puts vertice }
```

Este código cria uma classe `Grafo` que representa um grafo não direcionado. É possível adicionar vértices e arestas ao grafo, e em seguida realizar uma busca em largura a partir de um vértice inicial. A busca em largura retorna todos os vértices visitados durante o percurso.

No exemplo fornecido, são adicionados cinco vértices ("A", "B", "C", "D", "E") e quatro arestas. Em seguida, é realizada uma busca em largura a partir do vértice "A" e o caminho percorrido é exibido na saída.

Espero que este código atenda às suas expectativas!