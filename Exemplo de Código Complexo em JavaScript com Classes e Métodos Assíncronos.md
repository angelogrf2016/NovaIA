Aqui está um exemplo de um código complexo em JavaScript que envolve o uso de classes, métodos assíncronos e manipulação de elementos do DOM:

```javascript
class Pessoa {
  constructor(nome) {
    this.nome = nome;
  }

  async saudacao() {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve(`Olá, ${this.nome}!`);
      }, 2000);
    });
  }
}

async function mostrarSaudacao() {
  const nome = document.getElementById('nome').value;
  const pessoa = new Pessoa(nome);
  const saudacao = await pessoa.saudacao();
  document.getElementById('mensagem').textContent = saudacao;
}

document.getElementById('botao').addEventListener('click', mostrarSaudacao);
```

Neste exemplo, temos uma classe chamada `Pessoa` que tem um construtor para definir o nome da pessoa e um método `saudacao` assíncrono que retorna uma promessa que resolva com uma saudação usando o nome da pessoa.

A função `mostrarSaudacao` é um manipulador de eventos para o botão, que obtém o valor do campo de entrada com o `id` "nome", cria uma instância da classe `Pessoa`, aguarda a saudação assíncrona e, em seguida, atualiza o texto do elemento com o `id` "mensagem" com a saudação obtida.

Por fim, o evento de clique do botão é registrado para chamar a função `mostrarSaudacao` quando o botão for clicado.

Este é apenas um exemplo de um código complexo em JavaScript. A complexidade pode variar dependendo dos requisitos do projeto.