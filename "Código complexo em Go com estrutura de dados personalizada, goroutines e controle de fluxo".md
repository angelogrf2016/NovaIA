Claro! Aqui está um exemplo de código complexo em Go que envolve a criação de uma estrutura de dados personalizada, manipulação de goroutines e um fluxo de controle complexo:

```go
package main

import (
	"fmt"
	"sync"
)

type Counter struct {
	mu     sync.Mutex
	value  int
	cond   *sync.Cond
	wait   int
	notify int
	done   bool
}

func NewCounter() *Counter {
	c := &Counter{
		value:  0,
		cond:   sync.NewCond(&sync.Mutex{}),
		wait:   0,
		notify: 0,
		done:   false,
	}
	go c.worker()
	return c
}

func (c *Counter) worker() {
	for {
		c.cond.L.Lock()
		for c.wait == 0 && !c.done {
			c.cond.Wait()
		}
		if c.done {
			c.cond.L.Unlock()
			break
		}
		c.wait--
		c.value++
		c.cond.L.Unlock()

		fmt.Printf("Incrementing value to %d\n", c.value)

		c.cond.L.Lock()
		c.notify++
		c.cond.L.Unlock()
		c.cond.Broadcast()
	}
}

func (c *Counter) Increment() {
	c.cond.L.Lock()
	c.wait++
	c.cond.L.Unlock()
	c.cond.Signal()
}

func (c *Counter) Done() {
	c.cond.L.Lock()
	c.done = true
	c.cond.L.Unlock()
	c.cond.Signal()
}

func main() {
	c := NewCounter()

	var wg sync.WaitGroup
	wg.Add(10)

	for i := 0; i < 10; i++ {
		go func() {
			defer wg.Done()
			c.Increment()
		}()
	}

	wg.Wait()

	c.Done()

	fmt.Println("Final value:", c.value)
}
```

Neste exemplo, criamos uma estrutura de dados `Counter` que possui um valor inteiro, um mutex para controle de concorrência e um condicional para sincronização de goroutines. A estrutura também possui um flag `done` para indicar quando o processamento está concluído.

A função `NewCounter` é responsável por criar uma nova instância do `Counter` e iniciar uma goroutine chamada `worker`. O `worker` é um loop que aguarda a chegada de uma notificação para incrementar o valor do contador. Ele também verifica se o processamento está concluído e, nesse caso, encerra a goroutine.

As funções `Increment` e `Done` são usadas para notificar o `worker` sobre novas operações de incremento e o encerramento do processamento, respectivamente.

Na função `main`, criamos uma instância do `Counter` e iniciamos 10 goroutines que chamam a função `Increment`. Em seguida, aguardamos a conclusão dessas goroutines usando um `sync.WaitGroup`. Após isso, chamamos a função `Done` para sinalizar o fim do processamento.

Por fim, imprimimos o valor final do contador.