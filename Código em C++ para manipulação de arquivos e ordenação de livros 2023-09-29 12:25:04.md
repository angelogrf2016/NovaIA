Claro! Aqui está um exemplo de código complexo em C++ que envolve manipulação de arquivos, estruturas de dados e algoritmos de ordenação:

```cpp
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>

// Estrutura para representar um livro
struct Livro {
    std::string titulo;
    std::string autor;
    int ano;

    // Função de sobrecarga do operador de saída para imprimir os dados do livro
    friend std::ostream& operator<<(std::ostream& os, const Livro& livro) {
        os << "Título: " << livro.titulo << std::endl;
        os << "Autor: " << livro.autor << std::endl;
        os << "Ano: " << livro.ano << std::endl;
        return os;
    }
};

// Função para ordenar os livros por ano de publicação
bool compararPorAno(const Livro& livro1, const Livro& livro2) {
    return livro1.ano < livro2.ano;
}

int main() {
    // Abrir o arquivo de entrada
    std::ifstream arquivo("livros.txt");

    if (!arquivo) {
        std::cerr << "Erro ao abrir o arquivo." << std::endl;
        return 1;
    }

    // Ler os dados do arquivo e armazená-los em um vetor de livros
    std::vector<Livro> livros;
    std::string linha;

    while (std::getline(arquivo, linha)) {
        Livro livro;

        livro.titulo = linha;
        std::getline(arquivo, linha);
        livro.autor = linha;
        arquivo >> livro.ano;
        arquivo.ignore();

        livros.push_back(livro);
    }

    // Fechar o arquivo de entrada
    arquivo.close();

    // Ordenar os livros por ano de publicação
    std::sort(livros.begin(), livros.end(), compararPorAno);

    // Abrir o arquivo de saída
    std::ofstream arquivoOrdenado("livros_ordenados.txt");

    if (!arquivoOrdenado) {
        std::cerr << "Erro ao abrir o arquivo de saída." << std::endl;
        return 1;
    }

    // Escrever os livros ordenados no arquivo de saída
    for (const Livro& livro : livros) {
        arquivoOrdenado << livro << std::endl;
    }

    // Fechar o arquivo de saída
    arquivoOrdenado.close();

    std::cout << "Livros ordenados e salvos no arquivo 'livros_ordenados.txt'." << std::endl;

    return 0;
}
```

Neste código, estamos lendo os dados de livros de um arquivo de texto chamado "livros.txt", armazenando-os em um vetor de livros, ordenando-os por ano de publicação e, em seguida, escrevendo os livros ordenados em um novo arquivo chamado "livros_ordenados.txt".

Espero que isso atenda à sua solicitação de um código complexo em C++!