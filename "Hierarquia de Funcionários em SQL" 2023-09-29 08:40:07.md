Segue um exemplo de código SQL complexo:

```
WITH recursive cte_hierarchy AS (
    SELECT employee_id, employee_name, manager_id, 1 AS level
    FROM employees
    WHERE employee_id = 1 -- 1 é o ID do funcionário raiz
    
    UNION ALL
    
    SELECT e.employee_id, e.employee_name, e.manager_id, cte.level + 1
    FROM employees e
    INNER JOIN cte_hierarchy cte ON e.manager_id = cte.employee_id
)
SELECT employee_id, employee_name, level
FROM cte_hierarchy
ORDER BY level, employee_id;
```

Neste exemplo, estamos utilizando a cláusula WITH RECURSIVE para criar uma hierarquia de funcionários. A tabela "employees" possui colunas como "employee_id", "employee_name" e "manager_id", onde "employee_id" é a chave primária, "employee_name" é o nome do funcionário e "manager_id" é o ID do seu gerente.

O código começa selecionando o funcionário raiz com o ID 1 e definindo o nível como 1. Em seguida, fazemos uma união recursiva com a subconsulta, onde selecionamos todos os funcionários que possuem o ID do gerente igual ao ID do funcionário atual da hierarquia, incrementando o nível em 1.

Em seguida, fazemos uma consulta na tabela resultante da hierarquia, selecionando as colunas "employee_id", "employee_name" e "level" (nível) e ordenando os resultados pelo nível e pelo ID do funcionário.

Essa consulta retorna uma lista de todos os funcionários na hierarquia, juntamente com seus respectivos níveis.