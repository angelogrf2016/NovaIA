Claro! Aqui está um exemplo de código em Smalltalk que define uma classe chamada "Pessoa" e cria um objeto dessa classe:

```smalltalk
Object subclass: #Pessoa
    instanceVariableNames: 'nome idade'
    classVariableNames: ''
    poolDictionaries: ''
    category: 'Exemplo'

Pessoa >> initialize
    super initialize.
    nome := ''.
    idade := 0.

Pessoa >> setNome: aNome
    nome := aNome.

Pessoa >> setIdade: aIdade
    idade := aIdade.

Pessoa >> getNome
    ^ nome.

Pessoa >> getIdade
    ^ idade.
```

Agora, você pode criar um objeto "Pessoa" e definir seu nome e idade da seguinte maneira:

```smalltalk
| pessoa |
pessoa := Pessoa new.
pessoa setNome: 'João'.
pessoa setIdade: 25.
Transcript show: 'Nome: ', pessoa getNome; cr.
Transcript show: 'Idade: ', pessoa getIdade asString; cr.

```

Esse código criará um objeto "pessoa" da classe "Pessoa" com o nome "João" e a idade 25. Em seguida, ele imprimirá o nome e a idade da pessoa no console.