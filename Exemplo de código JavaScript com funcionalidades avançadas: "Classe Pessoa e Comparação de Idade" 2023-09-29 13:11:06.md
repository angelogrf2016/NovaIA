Aqui está um exemplo de código JavaScript complexo que usa várias funcionalidades avançadas da linguagem:

```javascript
// Definir uma classe chamada "Pessoa" com construtor e métodos
class Pessoa {
  constructor(nome, idade) {
    this.nome = nome;
    this.idade = idade;
  }

  saudacao() {
    console.log(`Olá, meu nome é ${this.nome} e eu tenho ${this.idade} anos.`);
  }

  static comparaIdade(pessoa1, pessoa2) {
    if (pessoa1.idade > pessoa2.idade) {
      console.log(`${pessoa1.nome} é mais velho(a) que ${pessoa2.nome}.`);
    } else if (pessoa1.idade < pessoa2.idade) {
      console.log(`${pessoa1.nome} é mais novo(a) que ${pessoa2.nome}.`);
    } else {
      console.log(`${pessoa1.nome} e ${pessoa2.nome} têm a mesma idade.`);
    }
  }
}

// Criar instâncias da classe Pessoa
const pessoa1 = new Pessoa("João", 25);
const pessoa2 = new Pessoa("Maria", 30);

// Chamar métodos e usar método estático da classe Pessoa
pessoa1.saudacao();
pessoa2.saudacao();
Pessoa.comparaIdade(pessoa1, pessoa2);
```

Neste código, estamos definindo uma classe chamada "Pessoa" com um construtor que recebe um nome e uma idade. A classe também possui um método de saudação que imprime uma mensagem no console.

Além disso, a classe tem um método estático chamado "comparaIdade", que compara a idade de duas instâncias da classe Pessoa e imprime uma mensagem com o resultado da comparação.

Em seguida, criamos duas instâncias da classe Pessoa, chamadas pessoa1 e pessoa2. Chamamos o método saudação de cada instância e, em seguida, chamamos o método estático "comparaIdade" passando as duas instâncias como argumentos.

Este é apenas um exemplo de código complexo em JavaScript, mas existem muitas outras funcionalidades avançadas que podem ser exploradas, como closures, arrow functions, promessas, entre outros.