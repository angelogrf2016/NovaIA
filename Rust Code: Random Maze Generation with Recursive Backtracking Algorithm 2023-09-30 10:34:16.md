Certainly! Here's a complex and unique Rust code that generates a random maze using the Recursive Backtracking algorithm:

```rust
use rand::Rng;

const WIDTH: usize = 20;
const HEIGHT: usize = 20;

struct Cell {
    x: usize,
    y: usize,
    walls: [bool; 4],
    visited: bool,
}

impl Cell {
    fn new(x: usize, y: usize) -> Self {
        Self {
            x,
            y,
            walls: [true; 4],
            visited: false,
        }
    }
}

fn generate_maze() -> Vec<Vec<bool>> {
    let mut maze = vec![vec![true; WIDTH * 2 + 1]; HEIGHT * 2 + 1];
    let mut stack: Vec<(usize, usize)> = Vec::new();

    let mut cells: Vec<Cell> = (0..HEIGHT)
        .flat_map(|y| (0..WIDTH).map(move |x| (x, y)))
        .map(|(x, y)| Cell::new(x, y))
        .collect();
    let start_cell = cells.first_mut().unwrap();
    start_cell.visited = true;
    stack.push((start_cell.x, start_cell.y));

    while let Some((x, y)) = stack.last().cloned() {
        let current_cell = cells.iter_mut().find(|cell| cell.x == x && cell.y == y).unwrap();
        let unvisited_neighbors: Vec<(usize, usize)> = vec![
            (x + 1, y),
            (x - 1, y),
            (x, y + 1),
            (x, y - 1),
        ]
        .into_iter()
        .filter(|&(nx, ny)| {
            nx < WIDTH && ny < HEIGHT && !cells.iter().any(|cell| cell.x == nx && cell.y == ny)
        })
        .collect();

        if unvisited_neighbors.is_empty() {
            stack.pop();
        } else {
            let (nx, ny) = *rand::thread_rng().choose(&unvisited_neighbors).unwrap();
            let next_cell = cells.iter_mut().find(|cell| cell.x == nx && cell.y == ny).unwrap();

            if nx > x {
                current_cell.walls[1] = false;
                next_cell.walls[3] = false;
            } else if nx < x {
                current_cell.walls[3] = false;
                next_cell.walls[1] = false;
            } else if ny > y {
                current_cell.walls[2] = false;
                next_cell.walls[0] = false;
            } else if ny < y {
                current_cell.walls[0] = false;
                next_cell.walls[2] = false;
            }

            next_cell.visited = true;
            stack.push((nx, ny));
        }
    }

    for cell in cells {
        let x = cell.x * 2 + 1;
        let y = cell.y * 2 + 1;
        maze[y][x] = false;

        if cell.walls[0] {
            maze[y - 1][x] = false;
        }
        if cell.walls[1] {
            maze[y][x + 1] = false;
        }
        if cell.walls[2] {
            maze[y + 1][x] = false;
        }
        if cell.walls[3] {
            maze[y][x - 1] = false;
        }
    }

    maze
}

fn main() {
    let maze = generate_maze();

    for row in maze {
        for cell in row {
            if cell {
                print!("█");
            } else {
                print!(" ");
            }
        }
        println!();
    }
}
```

This code uses the `rand` crate to generate a random maze using the Recursive Backtracking algorithm. The maze is represented as a 2D vector of booleans, where `true` represents a wall and `false` represents an open path. The maze is then printed to the console using ASCII characters.

Note that you'll need to add the `rand` crate to your `Cargo.toml` file to compile and run this code:

```toml
[dependencies]
rand = "0.8"
```

I hope you find this code complex and unique! Let me know if you have any questions.