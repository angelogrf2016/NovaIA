Claro! Aqui está um exemplo de código em Ruby que realiza uma simulação de um jogo de Poker:

```ruby
class Card
  attr_reader :rank, :suit
  
  def initialize(rank, suit)
    @rank = rank
    @suit = suit
  end
  
  def to_s
    "#{rank} of #{suit}"
  end
end

class Deck
  attr_reader :cards
  
  def initialize
    @cards = []
    build_deck
    shuffle
  end
  
  def build_deck
    ranks = %w[A 2 3 4 5 6 7 8 9 10 J Q K]
    suits = %w[Hearts Diamonds Clubs Spades]
    
    ranks.each do |rank|
      suits.each do |suit|
        @cards << Card.new(rank, suit)
      end
    end
  end
  
  def shuffle
    @cards.shuffle!
  end
  
  def deal(num)
    @cards.pop(num)
  end
end

class PokerGame
  attr_reader :deck, :players
  
  def initialize(num_players)
    @deck = Deck.new
    @players = []
    create_players(num_players)
  end
  
  def create_players(num)
    num.times do
      @players << []
    end
  end
  
  def deal_cards(num)
    @players.each do |player|
      cards = @deck.deal(num)
      player.concat(cards)
    end
  end
  
  def play
    deal_cards(5)
    show_players_hands
  end
  
  def show_players_hands
    @players.each_with_index do |player, index|
      puts "Player #{index + 1} hand:"
      player.each do |card|
        puts card.to_s
      end
      puts ""
    end
  end
end

game = PokerGame.new(4)
game.play
```

Neste código, temos três classes principais: `Card`, `Deck` e `PokerGame`.

A classe `Card` representa uma carta de baralho, com um valor (`rank`) e um naipe (`suit`). Ela tem um método `to_s` para exibir a carta em formato de string.

A classe `Deck` representa um baralho de cartas. Ela é responsável por criar todas as cartas do baralho, embaralhá-las e distribuí-las. Temos os métodos `build_deck` para criar o baralho, `shuffle` para embaralhar as cartas e `deal` para distribuir um número específico de cartas.

A classe `PokerGame` representa um jogo de Poker. Ela possui um baralho e uma lista de jogadores. O método `create_players` cria uma lista de jogadores com base no número passado como argumento. O método `deal_cards` distribui cartas para cada jogador. O método `play` chama o método `deal_cards` para distribuir as cartas e em seguida chama o método `show_players_hands` para exibir as mãos dos jogadores.

No final, criamos uma instância de `PokerGame` com 4 jogadores e chamamos o método `play` para iniciar o jogo.