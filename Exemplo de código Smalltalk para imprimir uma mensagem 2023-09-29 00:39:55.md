Claro! Aqui está um exemplo de código em Smalltalk que imprime uma mensagem na tela:

```smalltalk
| mensagem |
mensagem := 'Olá, mundo!'.
Transcript show: mensagem.
```

Este código cria uma variável chamada "mensagem" que armazena a string "Olá, mundo!". Em seguida, imprime a mensagem na janela Transcript usando o método "show:". Execute esse código em um ambiente Smalltalk para ver a mensagem impressa na tela.