Claro! Aqui está um exemplo de um código complexo em Go que envolve várias funcionalidades diferentes:

```go
package main

import (
	"fmt"
	"math"
	"sync"
)

func main() {
	// Calcula a raiz quadrada de cada número de 1 a 10 em paralelo
	var wg sync.WaitGroup
	wg.Add(10)

	for i := 1; i <= 10; i++ {
		go func(num int) {
			defer wg.Done()
			squareRoot := math.Sqrt(float64(num))
			fmt.Printf("A raiz quadrada de %d é %.2f\n", num, squareRoot)
		}(i)
	}

	wg.Wait()

	// Realiza uma operação matemática complexa
	result := complexOperation(5, 3)
	fmt.Println("O resultado da operação complexa é", result)
}

func complexOperation(a, b int) int {
	// Calcula o máximo divisor comum entre dois números
	for b != 0 {
		t := b
		b = a % b
		a = t
	}
	return a
}
```

Neste código, estamos calculando a raiz quadrada de cada número de 1 a 10 em paralelo usando goroutines e wait groups. Em seguida, realizamos uma operação matemática complexa para calcular o máximo divisor comum entre dois números.