Aqui está um exemplo de um código complexo em Swift que simula um jogo de Blackjack:

```swift
import Foundation

enum Suit: String {
    case hearts = "♥️"
    case diamonds = "♦️"
    case clubs = "♣️"
    case spades = "♠️"
}

enum Rank: String {
    case ace = "A"
    case two = "2"
    case three = "3"
    case four = "4"
    case five = "5"
    case six = "6"
    case seven = "7"
    case eight = "8"
    case nine = "9"
    case ten = "10"
    case jack = "J"
    case queen = "Q"
    case king = "K"
}

struct Card {
    let suit: Suit
    let rank: Rank
}

class Deck {
    private var cards: [Card]
    
    init() {
        cards = []
        for suit in Suit.allCases {
            for rank in Rank.allCases {
                cards.append(Card(suit: suit, rank: rank))
            }
        }
        shuffle()
    }
    
    func shuffle() {
        cards.shuffle()
    }
    
    func drawCard() -> Card? {
        if cards.isEmpty {
            return nil
        } else {
            return cards.removeFirst()
        }
    }
}

class Player {
    let name: String
    var hand: [Card]
    
    init(name: String) {
        self.name = name
        self.hand = []
    }
    
    func drawCard(from deck: Deck) {
        if let card = deck.drawCard() {
            hand.append(card)
        } else {
            print("The deck is empty.")
        }
    }
    
    func calculateHandValue() -> Int {
        var value = 0
        var hasAce = false
        for card in hand {
            switch card.rank {
            case .ace:
                value += 11
                hasAce = true
            case .two, .three, .four, .five, .six, .seven, .eight, .nine:
                value += Int(card.rank.rawValue)!
            case .ten, .jack, .queen, .king:
                value += 10
            }
        }
        if hasAce && value > 21 {
            value -= 10
        }
        return value
    }
}

class Game {
    let deck: Deck
    let player: Player
    let dealer: Player
    
    init() {
        deck = Deck()
        player = Player(name: "Player")
        dealer = Player(name: "Dealer")
    }
    
    func play() {
        player.drawCard(from: deck)
        dealer.drawCard(from: deck)
        player.drawCard(from: deck)
        dealer.drawCard(from: deck)
        
        print("Player's hand: \(handDescription(for: player))")
        print("Dealer's hand: \(handDescription(for: dealer, hideSecondCard: true))")
        
        var continueGame = true
        
        while continueGame {
            print("What would you like to do? (1) Hit (2) Stand")
            if let response = readLine(), let choice = Int(response) {
                switch choice {
                case 1:
                    player.drawCard(from: deck)
                    print("Player's hand: \(handDescription(for: player))")
                    if player.calculateHandValue() > 21 {
                        print("Player busts! Dealer wins.")
                        continueGame = false
                    }
                case 2:
                    print("Dealer's hand: \(handDescription(for: dealer))")
                    while dealer.calculateHandValue() < 17 {
                        dealer.drawCard(from: deck)
                        print("Dealer's hand: \(handDescription(for: dealer))")
                    }
                    if dealer.calculateHandValue() > 21 {
                        print("Dealer busts! Player wins.")
                    } else if dealer.calculateHandValue() > player.calculateHandValue() {
                        print("Dealer wins.")
                    } else if player.calculateHandValue() > dealer.calculateHandValue() {
                        print("Player wins.")
                    } else {
                        print("It's a tie.")
                    }
                    continueGame = false
                default:
                    print("Invalid choice.")
                }
            } else {
                print("Invalid input.")
            }
        }
    }
    
    func handDescription(for player: Player, hideSecondCard: Bool = false) -> String {
        var description = ""
        for (index, card) in player.hand.enumerated() {
            if index == 1 && hideSecondCard {
                description += "🎴"
            } else {
                description += "\(card.rank.rawValue)\(card.suit.rawValue) "
            }
        }
        return description
    }
}

let game = Game()
game.play()
```

Esse código cria uma simulação de um jogo de Blackjack onde o jogador joga contra o dealer. O jogador pode escolher entre "Hit" (pedir uma nova carta) ou "Stand" (ficar com a mão atual). O dealer vai continuar pedindo cartas até que sua mão tenha um valor igual ou maior que 17. O vencedor é aquele cuja mão possui o valor mais próximo de 21 sem exceder esse valor.