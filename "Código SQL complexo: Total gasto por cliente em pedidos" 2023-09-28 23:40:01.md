Aqui está um exemplo de um código SQL complexo que envolve várias tabelas, subconsultas e funções agregadas:

```sql
SELECT c.nome AS cliente, SUM(p.preco * i.quantidade) AS total_gasto
FROM clientes c
JOIN pedidos pe ON c.id = pe.cliente_id
JOIN itens_pedido i ON pe.id = i.pedido_id
JOIN produtos p ON i.produto_id = p.id
WHERE pe.data >= '2022-01-01' AND pe.data <= '2022-12-31'
GROUP BY c.nome
HAVING SUM(p.preco * i.quantidade) > 1000
ORDER BY total_gasto DESC;
```

Neste exemplo, temos as tabelas "clientes", "pedidos", "itens_pedido" e "produtos". A consulta retorna o nome do cliente e o valor total gasto por ele em pedidos realizados entre 01/01/2022 e 31/12/2022. A subconsulta envolve as tabelas relacionadas para obter o valor total gasto por cliente, utilizando a função agregada SUM para multiplicar o preço do produto pela quantidade de itens do pedido. Em seguida, é aplicada uma condição HAVING para filtrar apenas os clientes que gastaram mais de 1000 unidades monetárias. Por fim, os resultados são ordenados pelo valor total gasto em ordem decrescente.