Aqui está um código complexo em ADA que envolve várias funcionalidades diferentes:

```ada
with Ada.Text_IO, Ada.Integer_Text_IO;

procedure Complex_Code is
   type Array_Type is array (1..10) of Integer;
   Sum : Integer := 0;
   Average : Float := 0.0;
   
   procedure Read_Array (A : in out Array_Type) is
   begin
      for I in A'Range loop
         Ada.Text_IO.Put("Enter element " & Integer'Image(I) & ": ");
         Ada.Integer_Text_IO.Get(A(I));
      end loop;
   end Read_Array;
   
   procedure Print_Array (A : in Array_Type) is
   begin
      for I in A'Range loop
         Ada.Text_IO.Put(A(I)'Image & " ");
      end loop;
      Ada.Text_IO.New_Line;
   end Print_Array;
   
   procedure Calculate_Sum (A : in Array_Type) is
   begin
      for I in A'Range loop
         Sum := Sum + A(I);
      end loop;
   end Calculate_Sum;
   
   procedure Calculate_Average is
   begin
      Average := Float(Sum) / Float(Array_Type'Length);
   end Calculate_Average;
   
   procedure Sort_Array (A : in out Array_Type) is
      Temp : Integer := 0;
   begin
      for I in A'First + 1 .. A'Last loop
         for J in I - 1 .. A'First loop
            if A(J) > A(I) then
               Temp := A(I);
               A(I) := A(J);
               A(J) := Temp;
            end if;
         end loop;
      end loop;
   end Sort_Array;
   
   procedure Print_Sum is
   begin
      Ada.Text_IO.Put("Sum: ");
      Ada.Integer_Text_IO.Put(Sum);
      Ada.Text_IO.New_Line;
   end Print_Sum;
   
   procedure Print_Average is
   begin
      Ada.Text_IO.Put("Average: ");
      Ada.Text_IO.Put(Average, 2, 2);
      Ada.Text_IO.New_Line;
   end Print_Average;
   
   procedure Print_Menu is
   begin
      Ada.Text_IO.Put_Line("1. Read Array");
      Ada.Text_IO.Put_Line("2. Print Array");
      Ada.Text_IO.Put_Line("3. Calculate Sum");
      Ada.Text_IO.Put_Line("4. Calculate Average");
      Ada.Text_IO.Put_Line("5. Sort Array");
      Ada.Text_IO.Put_Line("6. Print Sum");
      Ada.Text_IO.Put_Line("7. Print Average");
      Ada.Text_IO.Put_Line("8. Exit");
   end Print_Menu;
   
   Choice : Integer;
   Array_Var : Array_Type;
begin
   loop
      Print_Menu;
      Ada.Text_IO.Put("Enter your choice: ");
      Ada.Integer_Text_IO.Get(Choice);
      
      case Choice is
         when 1 =>
            Read_Array(Array_Var);
         when 2 =>
            Print_Array(Array_Var);
         when 3 =>
            Calculate_Sum(Array_Var);
         when 4 =>
            Calculate_Average;
         when 5 =>
            Sort_Array(Array_Var);
         when 6 =>
            Print_Sum;
         when 7 =>
            Print_Average;
         when 8 =>
            exit;
         when others =>
            Ada.Text_IO.Put_Line("Invalid choice");
      end case;
   end loop;
end Complex_Code;
```

Este código implementa um programa que permite ao usuário ler um array de 10 elementos, imprimir o array, calcular a soma e a média dos elementos, classificar o array em ordem crescente, imprimir a soma e a média, e sair do programa. O usuário pode escolher qual operação realizar através de um menu de opções.