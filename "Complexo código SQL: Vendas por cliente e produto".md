Aqui está um exemplo de código complexo em SQL que envolve várias tabelas e operações:

```
SELECT c.nome AS cliente, p.nome AS produto, SUM(v.quantidade) AS total_vendas
FROM clientes c
JOIN vendas v ON c.id = v.id_cliente
JOIN produtos p ON v.id_produto = p.id
WHERE v.data_venda BETWEEN '2022-01-01' AND '2022-12-31'
GROUP BY c.nome, p.nome
HAVING total_vendas > 100
ORDER BY total_vendas DESC;
```

Neste código, estamos selecionando o nome do cliente, nome do produto e a soma total de vendas para cada cliente e produto específico durante um determinado período de tempo. Estamos juntando as tabelas `clientes`, `vendas` e `produtos` usando as chaves estrangeiras correspondentes. Em seguida, filtramos as vendas dentro do período especificado usando a cláusula `WHERE`. Agrupamos os resultados por cliente e produto usando a cláusula `GROUP BY`. Em seguida, filtramos os resultados para mostrar apenas as vendas com mais de 100 unidades usando a cláusula `HAVING`. Por fim, ordenamos os resultados pela soma total de vendas em ordem decrescente usando a cláusula `ORDER BY`.